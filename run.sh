#!/bin/bash

source .env

MODE=-td

if which nvidia-smi &> /dev/null;then
  export GPU_OPTS="--gpus all"
else
  export GPU_OPTS=
fi

docker container run ${RUN_OPTS} ${GPU_OPTS} ${CONTAINER_NAME} ${MODE} ${NETWORK} ${PORT_MAP} ${VOL_MAP} ${REGISTRY}${IMAGE}${TAG}
