#!/bin/sh

if [ -d /etc/apt ]; then
        [ -n "$http_proxy" ] && echo "Acquire::http::proxy \"${http_proxy}\";" > /etc/apt/apt.conf; \
        [ -n "$https_proxy" ] && echo "Acquire::https::proxy \"${https_proxy}\";" >> /etc/apt/apt.conf; \
        [ -f /etc/apt/apt.conf ] && cat /etc/apt/apt.conf
fi


apt-get update -y && apt-get install -y \
        wget \
        build-essential \
        vim \
        git \
        curl \
        graphviz \
        libgraphviz-dev \
        apt-transport-https \
        ca-certificates

pip install --upgrade pip
