import tensorflow as tf
import numpy as np
from bayesmodels.core_models import BayesianGP
from bayesmodels.kernels import *
from tensorflow_probability.python import distributions  as tfd
import logging
import matplotlib.pyplot as plt
logger = tf.get_logger()
logger.setLevel(logging.ERROR)
import pickle
import os

type = np.float32




class testBayesianGP(tf.test.TestCase):

    def setUp(self):
        self.train_inputs = np.loadtxt('../testData/Xtrain.txt')
        self.train_outputs = np.loadtxt('../testData/Ytrain.txt')
        # normalizing the inputs and outputs
        mean_x = np.mean(self.train_inputs, axis = 0)
        std_x = np.std(self.train_inputs, axis = 0, keepdims = True)
        self.train_inputsnorm = (self.train_inputs - mean_x)/std_x
        mean_y = np.mean(self.train_outputs)
        std_y = np.std(self.train_outputs)
        self.train_outputsnorm = (self.train_outputs - mean_y)/std_y
        self.scaling =[[mean_x, std_x],[mean_y, std_y]]
        return

    def test_initialize(self):
        noise_level =  5e-4
        name= 'test_model'
        for kernel_type in stationary_kernel_mapping:
            model = BayesianGP(self.train_inputsnorm,self.train_outputsnorm,
                kernel_type= kernel_type, noise_level = 1e-3, name = name)
        return

    def test_get_batch_and_event_shape(self):
        noise_level = 2e-4
        name = 'test_model'
        kernel_type='RBF'
        model = BayesianGP(self.train_inputsnorm,self.train_outputsnorm,
            kernel_type= kernel_type, noise_level = noise_level, name = name)
        info_dict = model.get_batch_and_event_shape()
        print(info_dict)
        return

    def test_get_initial_state(self):
        noise_level = 2e-4
        name = 'test_model'
        kernel_type='RBF'
        model = BayesianGP(self.train_inputsnorm,self.train_outputsnorm,
            kernel_type= kernel_type, noise_level = noise_level, name = name)
        info_dict = model.get_initial_state()
        print(info_dict)
        return

    def test_update_prior(self):
        noise_level = 2e-4
        name = 'test_model'
        kernel_type='Matern52'
        model = BayesianGP(self.train_inputsnorm,self.train_outputsnorm,
            kernel_type= kernel_type, noise_level = noise_level, name = name)
        new_loc_prior = tfd.StudentT(df = 5, loc = 0.5, scale = 0.56)
        model.update_prior('test_model/loc', new_loc_prior)
        self.assertEqual(new_loc_prior, model.rv_loc)
        new_vark_prior = tfd.Exponential(rate = 1.7)
        model.update_prior('test_model/kernel/vark', new_vark_prior)
        self.assertEqual(new_vark_prior, model.K.priors_list[1])
        return

    def test_update_initial_state(self):
        noise_level = 2e-4
        name = 'test_model'
        kernel_type='Matern52'
        model = BayesianGP(self.train_inputsnorm,self.train_outputsnorm,
            kernel_type= kernel_type, noise_level = noise_level, name = name)
        new_loc_state = -1.5
        model.update_initial_state('test_model/loc', new_loc_state)
        self.assertEqual(new_loc_state, model.loc_initial_state)
        new_vark_state = 6.0
        model.update_initial_state('test_model/kernel/vark', new_vark_state)
        self.assertEqual(new_vark_state, model.K.get_initial_state()[1])
        return



    def test_mcm(self):
        kernel_type='RBF'
        noise_level = 1e-3
        name = 'test_model'
        mcmc_samples = 4000
        num_burnin_steps = 5000
        num_adaptation_steps = 4000
        adaption_rate = 0.60
        init_step_size = 0.20
        num_leapfrog_steps = 3
        thinning = 3
        model = BayesianGP(self.train_inputsnorm,self.train_outputsnorm,
            kernel_type= kernel_type, noise_level = noise_level, name = name)
        hyp_samples, average_acceptance_ratio = model.mcmc(mcmc_samples, num_burnin_steps,
                                            num_adaptation_steps,
                                            adaption_rate,
                                            init_step_size,
                                            num_leapfrog_steps,
                                            thinning)

        output_directory= '../test_results/'
        beta_samples= hyp_samples[0].numpy()
        plt.figure(figsize=(20,10))
        plt.plot(beta_samples[:,0])
        figpath = 'beta.png'
        figpath = os.path.join(output_directory, figpath)
        plt.savefig(figpath)

        vark_samples = hyp_samples[1].numpy()
        plt.figure(figsize=(20,10))
        plt.plot(vark_samples)
        figpath = 'vark.png'
        figpath = os.path.join(output_directory, figpath)
        plt.savefig(figpath)

        loc_samples = hyp_samples[-1].numpy()
        plt.figure(figsize=(20,10))
        plt.plot(loc_samples)
        figpath = 'loc.png'
        figpath = os.path.join(output_directory, figpath)
        plt.savefig(figpath)

        print('Average acceptance ratio: ', average_acceptance_ratio.numpy())

        # dict_samples = {}
        # n = len(model.hypnames_list)
        # for i in range(n):
        #     name = model.hypnames_list[i]
        #     dict_samples[name] = hyp_samples[i].numpy()
        # output_file ='../test_results/samples.pkl'
        # pickle_out = open(output_file, 'wb')
        # pickle.dump(dict_samples, pickle_out)
        # pickle_out.close()

        return

    def test_predict(self):
        kernel_type='RBF'
        noise_level = 1e-3
        name = 'test_model'
        # loading saved samples
        input_file ='../test_results/samples.pkl'
        pickle_in = open(input_file, 'rb')
        dict_samples = pickle.load(pickle_in)
        pickle_in.close()
        print(dict_samples.keys())
        model = BayesianGP(self.train_inputsnorm,self.train_outputsnorm,
            kernel_type= kernel_type, noise_level = noise_level, name = name)
        n = len(model.hypnames_list)
        hyp_samples =[]
        for i in range(n):
            name = model.hypnames_list[i]
            hyp_samples.append(tf.convert_to_tensor(dict_samples[name], tf.float32))

        mean_pos, var_pos = model.predict(self.train_inputsnorm, hyp_samples)
        std_pos = np.sqrt(var_pos)
        # converting to the right scale
        mean_y, std_y = self.scaling[1]
        mean_posf = mean_pos*std_y + mean_y
        std_posf = std_pos*std_y
        plt.figure(figsize =(20,10))
        plt.scatter(self.train_inputs,self.train_outputs,marker='x', color = 'black', label = 'actual')
        plt.plot(self.train_inputs, mean_posf, color = 'green', label = 'predicted')
        plt.vlines(self.train_inputs, mean_posf -2*std_posf,  mean_posf + 2*std_posf, color = 'red', alpha = 0.8)
        plt.grid()
        plt.legend()
        output_directory= '../test_results/'
        figpath = 'predicted_vs_actual.png'
        figpath = os.path.join(output_directory, figpath)
        plt.savefig(figpath)

        return


if __name__ == "__main__":
    tf.test.main()
