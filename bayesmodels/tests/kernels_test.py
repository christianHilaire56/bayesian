import tensorflow as tf
import numpy as np
from bayesmodels.kernels import *
from tensorflow_probability.python import distributions  as tfd
import logging
logger = tf.get_logger()
logger.setLevel(logging.ERROR)

type = np.float32



class KernelUnitTest(tf.test.TestCase):

    def setUp(self):
        pass

    def test_kernel_shape(self):
        d = np.random.randint(low=2, high=15)
        n1 = np.random.randint(low=12, high=50)
        n2 = np.random.randint(low=5, high=20)
        X1 = np.random.randn(n1,d)
        X2 = np.random.randn(n2,d)
        beta = np.random.uniform(low=0.5,high=10, size = d)
        beta = beta.astype(type)
        var = 3.1
        hyps = [beta, var]
        X1 = X1.astype(type)
        X2 = X2.astype(type)
        name = 'test_case'
        for key in stationary_kernel_mapping:
            Kclass = stationary_kernel_mapping[key]
            K = Kclass(name, d)
            K12 = K.compute(X1, X2, hyps,ref_index=0)
            message = 'kernel ' + key + ' outputs the wrong shape'
            self.assertShapeEqual(np.zeros((n1,n2), dtype =type),K12, msg = message)
            hyps_log = K.hyp_loglikelihood(hyps)
        return

    def test_update_initial_state(self):
        d = np.random.randint(low=2, high=15)
        name ='kernel'
        K = RBF(name, d)
        new_beta = tf.random.uniform(shape=[d], minval = 0.7, maxval=2.3)
        expected_list = [new_beta, K.initial_state_list[1]]
        K.update_initial_state('beta', new_beta )
        self.assertEqual(K.get_initial_state(), expected_list)
        new_vark = 1.8
        K.update_initial_state('vark', new_vark )
        expected_list = [new_beta, new_vark]
        self.assertEqual(K.get_initial_state(), expected_list)
        return

    def test_update_prior(self):
        d = np.random.randint(low=2, high=15)
        name ='kernel'
        K = Matern32(name, d)
        new_beta_prior = tfd.Independent(tfd.Gamma(concentration = 7*tf.ones(d, tf.float32),
		 				rate = 12*tf.ones(d, tf.float32)),
		                   reinterpreted_batch_ndims=1, name= 'new_rv_beta')
        K.update_prior('beta', new_beta_prior)
        self.assertEqual(K.priors_list[0], new_beta_prior)
        new_vark_prior = tfd.Exponential(rate = 1.7)
        K.update_prior('vark', new_vark_prior)
        self.assertEqual(K.priors_list[1], new_vark_prior)
        return


    def test_RBF(self):
        return

    def test_Matern12(self):
        return

    def test_Matern32(self):
        return

    def test_Matern52(self):
        return


if __name__ == "__main__":
    tf.test.main()
