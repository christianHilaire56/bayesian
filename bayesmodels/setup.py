import os
import re
import sys

from setuptools import find_packages, setup

# Dependencies
requirements = [
    'numpy>=1.17.2',
    'scipy>=1.3.1',
    'pandas>=0.25.1',
    'matplotlib>=3.1.1',
    'tensorflow_probability>=0.8.0'
]

packages = find_packages('.')

setuptools.setup(
    name="bayesmodels",
    version="0.0.1",
    author="Christian Hilaire",
    author_email="christian.hilaire22@gmail.com",
    description="Bayesian models with tensorflow_probability",
    packages=packages,
    install_requires=requirements,
    classifiers=[
          'Natural Language :: English',
          'Operating System :: MacOS :: MacOS X',
          'Operating System :: Microsoft :: Windows',
          'Operating System :: POSIX :: Linux',
          'Programming Language :: Python :: 3.5',
          'Programming Language :: Python :: 3.6',
          'Programming Language :: Python :: 3.7',
          'Topic :: Scientific/Engineering :: Artificial Intelligence'
          ]
)
