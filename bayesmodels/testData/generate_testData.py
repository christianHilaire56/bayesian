import numpy as np


if __name__ == "__main__":
    # test data for bayesian gp
    train_inputs = np.arange(2,20,0.2)
    train_outputs = 0.1*np.square(train_inputs) + \
                        8*np.sin(2*train_inputs/3) + \
                        0.5*np.random.randn(len(train_inputs))

    np.savetxt('./Xtrain.txt', train_inputs)
    np.savetxt('./Ytrain.txt', train_outputs)
