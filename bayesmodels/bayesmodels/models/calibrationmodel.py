import os
import numpy as np
import tensorflow as tf
from ..core_models import Calibration
from ..sensitivity import sensitivity, labelling
import matplotlib.pyplot as plt
import math
import pandas as pd


class CalibrationModel(Calibration):

    def __init__(self, sim_inputs, sim_outputs, exp_inputs, exp_outputs, model_info = None, kernel_types = ['RBF', 'RBF', 'RBF'], noise_level = 1e-3, labels = [], output_label = None):
        # Inputs:
        #   sim_inputs_ := Nsim x Dtotal numpy array of simulation inputs and calibration parameter values.
        #           The first columns must correspond to the input variables and the remaining
        #           columns must correspond to the calibration parameters.
        #   sim_outputs := Nsim-dimensional numpy vector of outputs
        #   exp_inputs := Nexp x Dfeatures numpy array of experimental input values. The variable columns
        #             must have the same order as the input variable columns of sim_inputs_pars
        #  exp_outputs := Nexp-dimensional numpy vector of outputs
        #               1) a dictionary containing the hyperparameter samples.
        #               2) the value of the noise variance
        #               3) the type of kernel used
        #               4) the labels of the variables
        #               5) the normalized training data
        #               6) the scaling for the data
        #               7) a dictionary containing sensitivity calculation info
        #                    executed so far
        # kernel_types := list of strings specifying the type of kernels to be used. Options are
		#                'RBF', 'Matern12', 'Matern32', 'Matern52'
        # noise_level := variance of the Gaussian noise for the normalized data
        # labels:= list containing labels for the input variables and the calibration parameters. A default list is
        #       generated if this is not specified
        # output_label := label for the output variable

        name = "calibration_model"
        if model_info:
            print("Retrieving model info from previous run.")
            try:
                self.hyperpar_samples = copy.deepcopy(model_info['samples'])
                noise_level = model_info['noise_level']
                self.kernel_types = model_info['kernel_types']
                self.labels = model_info['labels']
                self.output_label = model_info['output_label']
                sim_inputs_norm = model_info['sim_inputs_norm']
                sim_outputs_norm = model_info['sim_outputs_norm']
                exp_inputs_norm = model_info['exp_inputs_norm']
                exp_outputs_norm = model_info['exp_outputs_norm']
                self.scaling = model_info['scaling']
                self.sensitivity_info = model_info['sensitivity_info']
            except Exception as e:
                traceback.print_exc()
                print('Failed to retrieve model info.')
        else:
            self.hyperpar_samples = {}
            self.kernel_types = kernel_types
            if (noise_level > 1) or (noise_level < 0):
                raise Exception('Invalid value for the noise_level: ' + str(noise_level) + '. It should be between 0 and 1.')
             # checking type of inputs and outputs
            checking_type = isinstance(sim_inputs, np.ndarray) and isinstance(sim_outputs, np.ndarray) and isinstance(exp_inputs, np.ndarray) and isinstance(exp_outputs, np.ndarray)
            if not(checking_type):
                raise TypeError('sim_inputs, sim_outputs, exp_inputs and exp_outputs must all be numpy arrays.')
            if len(sim_inputs.shape) == 1:
                raise Exception('Array sim_inputs_pars of simulator input and calibration parameter must have at least 2 columns')
            if len(exp_inputs.shape) == 1:
                exp_inputs_used = exp_inputs[:,None]
            else:
                exp_inputs_used = exp_inputs
            n_features = exp_inputs_used.shape[1]
            n_pars = sim_inputs.shape[1] - n_features  # number of calibration parameters
            if n_pars <= 0:
                raise Exception('Computed number of calibration parameters is less than or equal to 0! Array sim_inputs is supposed to have more columns than array exp_inputs.')
            # normalizing the data
            #1) features and calibration parameters
            mean_sim = np.mean(sim_inputs, axis = 0)
            std_sim = np.std(sim_inputs, axis = 0, keepdims = True)
            sim_inputs_norm  = (sim_inputs - mean_sim)/std_sim
            exp_inputs_norm = (exp_inputs_used - mean_sim[:n_features])/std_sim[0,:n_features]
            #2) outputs
            mean_y = np.mean(sim_outputs)
            std_y = np.std(sim_outputs)
            sim_outputs_norm = (sim_outputs - mean_y)/std_y
            exp_outputs_norm = (exp_outputs - mean_y)/std_y
            self.scaling= [[mean_sim, std_sim], [mean_y, std_y]]
            if labels == []:
                self.features_labels = ['x' + str(i) for i in range(n_features)]
                self.par_labels  = ['p' + str(i) for i in range(n_pars)]
                self.labels = self.features_labels + self.par_labels
            elif  (len(labels) != n_features + n_pars) or not(all(isinstance(s, str) for s in labels)):
                raise Exception('Invalid input for labels')
            else:
                self.labels = labels
                self.features_labels = labels[:n_features]
                self.par_labels = labels[n_features:]
            if output_label == None:
                self.output_label = 'y'
            else:
                self.output_label = output_label
        # The normalized calibration parameters will be given uniform distributions. We need to specify the bounds for these distributions.
        lower_bounds = np.min(sim_inputs_norm[:,n_features:], axis = 0).tolist()
        upper_bounds = np.max(sim_inputs_norm[:,n_features:], axis = 0).tolist()

        Calibration.__init__(self, sim_inputs_norm, sim_outputs_norm, exp_inputs_norm, exp_outputs_norm, lower_bounds, upper_bounds, kernel_types, noise_level, name)

        # Bounds needed for sensitivity analysis
        self.doe_bounds = np.zeros((sim_inputs_norm.shape[1],2))
        self.doe_bounds[:,0] = np.min(sim_inputs_norm, axis = 0)
        self.doe_bounds[:,1] = np.max(sim_inputs_norm, axis = 0)
        return


    def get_model_info(self):
        model_info = {}
        model_info['samples'] = copy.deepcopy(self.hyperpar_samples)
        model_info['kernel_types']= self.kernel_types
        model_info['noise_level'] = self.noise
        model_info['labels'] = self.labels
        model_info['output_label']= self.output_label
        model_info['sim_inputs_norm'] = sim_inputs_norm
        model_info['sim_outputs_norm']= sim_outputs_norm
        model_info['exp_inputs_norm'] = exp_inputs_norm
        model_info['exp_outputs_norm'] = exp_outputs_norm
        model_info['scaling'] = self.scaling
        return model_info


    def run_mcmc(self,mcmc_samples, num_burnin_steps, num_adaptation_steps, adaptation_rate = 0.40, init_step_size = 0.01, num_leapfrog_steps = 3,thinning = 2):
        # Function used to perform the sampling for the posterior distributions of the hyperparameters
		# Inputs:
		#	mcmc_samples := number of samples to collect for the hyperparameters
		#	num_burnin_steps := number of samples to discard
		#  num_adaptation_steps := number of steps used to execute adaptive HMC
		# 	init_step_size :=  initial step_size for the HMC sampler
		# 	num_leapfrog_steps := number of leapfrog steps for the HMC sampler
		# thinning := number of mcmc samples to skip in between before storing
		# Outputs:
		#	hyp_samples= dictionary containing samples of the hyperparmeters
		#   acceptance_rate_ := acceptance rate of the sampling

        hypar_samples, average_acceptance_ratio = self.mcmc(mcmc_samples, num_burnin_steps,
                                                        num_adaptation_steps, adaptation_rate,
                                                        init_step_size, num_leapfrog_steps,thinning)

        # 1) simulation part
        self.hyperpar_samples['simulator_features_kernel_inverse_lengthscales'] = hypar_samples[0].numpy()
        self.hyperpar_samples['simulator_variance'] = hypar_samples[1].numpy()
        self.hyperpar_samples['simulator_par_kernel_inverse_lengthscales'] = hypar_samples[2].numpy()
        self.hyperpar_samples['simulator_gp_constant_mean_function'] = hypar_samples[-1].numpy()
        #2) discrepancy part
        self.hyperpar_samples['discrepancy_kernel_inverse_lengthscales'] = hypar_samples[3].numpy()
        self.hyperpar_samples['discrepancy_variance'] = hypar_samples[4].numpy()
        # 3) calibration parameters
        self.hyperpar_samples['normalized_calibration_parameters'] = hypar_samples[-2].numpy()

        print('Average acceptance ratio: ', average_acceptance_ratio.numpy())
        return


    def plot_chains(self, display = True, save_plot = False,  directory_path = None):
        # Function used to plot the chains from the  mcmc sampling and scatter plot
        # for the calibration parameters
        # Inputs:
        #   directory_path := directory where to save the mcmc samples plot. It defaults to the current directory if not specified
        if len(self.hyperpar_samples) == 0:
            raise Exception('Hyperparameter and calibration parameter samples must be generated or retrieved first.')

        # plotting the mcmc chains
        nplots = 2*self.dim_features + 2*self.dim_par + 3
        fig, axes = plt.subplots(nplots, 1, figsize=(20, 2.0*nplots),sharex=True)

        # 1) Plotting and saving the chains for the simulation part
        betasx_samples = self.hyperpar_samples['simulator_features_kernel_inverse_lengthscales']
        mcmc_samples = len(betasx_samples)
        t = np.arange(mcmc_samples)
        k = -1
        for i in range(self.dim_features):
            k = k+1
            axes[k].plot(t,betasx_samples[:,i])
            title =  self.features_labels[i] + '_simulator_kernel_inverse_lengthscale_samples'
            axes[k].set_title(title)
        betaspar_samples = self.hyperpar_samples['simulator_par_kernel_inverse_lengthscales']
        for i in range(self.dim_par):
            k = k+1
            axes[k].plot(t,betaspar_samples[:,i])
            title =  self.par_labels[i] + '_simulator_kernel_inverse_lengthscale_samples'
            axes[k].set_title(title)
        varsim_samples = self.hyperpar_samples['simulator_variance']
        k = k+1
        axes[k].plot(t,varsim_samples)
        title = 'simulator_kernel_variance_samples'
        axes[k].set_title(title)
        loc_samples = self.hyperpar_samples['simulator_gp_constant_mean_function']
        k = k+1
        axes[k].plot(t,loc_samples)
        title = 'simulator_constant_mean_function_samples'
        axes[k].set_title(title)
        # 2) dicrepancy part
        betad_samples = self.hyperpar_samples['discrepancy_kernel_inverse_lengthscales']
        for i in range(self.dim_features):
            k = k+1
            axes[k].plot(t,betad_samples[:,i])
            title =  self.features_labels[i] + '_discrepancy_inverse_lengthscale_samples'
            axes[k].set_title(title)
        vard_samples = self.hyperpar_samples['discrepancy_variance']
        k = k+1
        axes[k].plot(t,vard_samples)
        title = 'discrepancy_kernel_variance_samples'
        axes[k].set_title(title)
        # We first need to convert to the proper scale
        mean_sim, std_sim = self.scaling[0]
        par_samples = self.hyperpar_samples['normalized_calibration_parameters']
        par_samples = par_samples*std_sim[0,self.dim_features:] + mean_sim[self.dim_features:]
        for i in range(self.dim_par):
            k = k+1
            axes[k].plot(t,par_samples[:,i])
            title = self.par_labels[i] + '_samples'
            axes[k].set_title(title)

        if save_plot:
            if directory_path == None:
                directory_path = os.getcwd()
            if not(os.path.isdir(directory_path)):
                raise Exception('Invalid directory path ', directory_path)
            figpath ='mcmc_chains.png'
            figpath = os.path.join(directory_path, figpath)
            plt.savefig(figpath)
        if display:
            plt.show()
        else:
            plt.close()
        # scatter plot for the calibration parameters
        df = pd.DataFrame(par_samples, columns = self.par_labels)
        if save_plot:
            if directory_path == None:
                directory_path = os.getcwd()
            if not(os.path.isdir(directory_path)):
                raise Exception('Invalid directory path ', directory_path)
            figpath ='par_scatter.png'
            figpath = os.path.join(directory_path, figpath)
            plt.figure(figsize=(12,12))
            axs = pd.plotting.scatter_matrix(df)
            for i in range(len(self.par_labels)):
                axs[i,i].set_ylabel(self.par_labels[i] + '_frequency')
            plt.savefig(figpath)
        if display:
            plt.show()
        else:
            plt.close()
        return

    def get_parameter_correlations_plot(self, display = True, save_plot = False,  directory_path = None):
        if len(self.hyperpar_samples) == 0:
            raise Exception('Hyperparameter and calibration parameter samples must be generated or retrieved first.')
        mean_sim, std_sim = self.scaling[0]
        par_samples = self.hyperpar_samples['normalized_calibration_parameters']
        par_samples = par_samples*std_sim[0,self.dim_features:] + mean_sim[self.dim_features:]
        # scatter plot for the calibration parameters
        df = pd.DataFrame(par_samples, columns = self.par_labels)
        plt.figure(figsize=(12,12))
        axs = pd.plotting.scatter_matrix(df)
        for i in range(len(self.par_labels)):
            axs[i,i].set_ylabel(self.par_labels[i] + '_frequency')
        if save_plot:
            if directory_path == None:
                directory_path = os.getcwd()
            if not(os.path.isdir(directory_path)):
                raise Exception('Invalid directory path ', directory_path)
            figpath ='par_scatter.png'
            figpath = os.path.join(directory_path, figpath)
            plt.savefig(figpath)
        if display:
            plt.show()
        else:
            plt.close()
        return


    def get_predictions(self, Xtest, type = "full", fast = False):
        # Computes approximate values of the full posterior mean and variance of the Gaussian process
		# by using samples of the posterior distribution of the hyperparameters
		#Inputs:
		#	Xtest :=  N x D input array
        #   type := the type of GP ("full", "simulator", "discrepancy")
		#	fast = Boolean specifying if we use just median values of the hyperparmeters
        #          or if all the samples are used.
		# Outputs:
		#	mean_pos, std_pos := the mean and standard deviation for the posterior Gaussian process (arrays of shape N)

        if len(self.hyperpar_samples) == 0:
            raise Exception('Hyperparameter samples must be generated or retrieved first.')

        mean_sim, std_sim = self.scaling[0]
        mean_y, std_y = self.scaling[1]
        mean_x = mean_sim[:self.dim_features]
        std_x = std_sim[:,:self.dim_features]

        if len(Xtest.shape) == 1:
            Xnew = Xtest[:, None]
        else:
            Xnew = Xtest
        Xtest_norm = (Xnew - mean_x)/std_x
        Xtest_norm = tf.convert_to_tensor(Xtest_norm, tf.float32)

        betasx_samples = self.hyperpar_samples['simulator_features_kernel_inverse_lengthscales']
        varsim_samples  = self.hyperpar_samples['simulator_variance']
        betaspar_samples = self.hyperpar_samples['simulator_par_kernel_inverse_lengthscales']
        betadx_samples = self.hyperpar_samples['discrepancy_kernel_inverse_lengthscales']
        vardisc_samples = self.hyperpar_samples['discrepancy_variance']
        par_samples = self.hyperpar_samples['normalized_calibration_parameters']
        loc_samples = self.hyperpar_samples['simulator_gp_constant_mean_function']
        temp = [betasx_samples, varsim_samples, betaspar_samples, betadx_samples, vardisc_samples, par_samples, loc_samples]
        hypar_samples = []
        for i in range(7):
            entry = temp[i]
            if fast:
                entry = np.median(entry, axis = 0, keepdims = True)
            entry = tf.convert_to_tensor(entry, tf.float32)
            hypar_samples.append(entry)
        mean_pos, var_pos = self.predict(Xtest_norm, hypar_samples, type)

        mean_pos = mean_pos.numpy()[:,0]
        var_pos = var_pos.numpy()[:,0]
        std_pos = np.sqrt(var_pos)
        # Converting to the proper scale
        mean_pos = mean_pos*std_y
        std_pos = std_pos*std_y
        if type in ["full", "simulator"]:
            mean_pos += mean_y
        return mean_pos, std_pos
