import os
import numpy as np
import tensorflow as tf
from ..core_models import BayesianGP
from ..sensitivity import sensitivity, labelling
import matplotlib.pyplot as plt
import math

class BGPModel(BayesianGP):

    def __init__(self, inputs=None, outputs =None, model_info = None, kernel_type = 'RBF', noise_level = 1e-3, input_labels = [], output_label = None):
        # Inputs:
        #   inputs := N x D numpy array of inputs
        #   outputs := N-dimensional numpy vector of outputs
        #   model_info = dictionary containing
        #               1) a dictionary containing the hyperparameter samples.
        #               2) the value of the noise variance
        #               3) the type of kernel used
        #               4) the labels of the variables
        #               5) the normalized training data
        #               6) the scaling for the data
        #               7) a dictionary containing sensitivity calculation info
        #                    executed so far
        #    kernel_type := string specifying the type of kernel to be used. Options are
		#                'RBF', 'Matern12', 'Matern32', 'Matern52' and 'RationalQuadratic'
        #   noise_level := variance of the Gaussian noise for the normalized data
        #   input_labels:= list containing labels for the input variables. A default list is
        #           generated if this is not specified
        #  output_label := label for the output variable

        name = 'bgp_model'
        if model_info:
            print("Retrieving model info from previous run.")
            try:
                self.hyperpar_samples = copy.deepcopy(model_info['samples'])
                noise_level = model_info['noise_level']
                self.kernel_type = model_info['kernel_type']
                self.input_labels = model_info['input_labels']
                self.output_label = model_info['output_label']
                Xnorm = model_info['Xnorm']
                Ynorm = model_info['Ynorm']
                self.scaling = model_info['scaling']
                self.sensitivity_info = model_info['sensitivity_info']
            except Exception as e:
                traceback.print_exc()
                print('Failed to retrieve model info.')
        else:
            self.hyperpar_samples = {}
            # Checking that the Gaussian noise variance is between 0 and 1
            if (noise_level > 1) or (noise_level < 0):
                raise Exception('Invalid value for the noise_level: ' + str(noise_level) + '. It should be between 0 and 1.')
            self.kernel_type = kernel_type
            # checking type of inputs and outputs
            checking_type = isinstance(inputs, np.ndarray) and isinstance(outputs, np.ndarray)
            if not(checking_type):
                raise TypeError('inputs and outputs must be numpy array')
            if len(inputs.shape) == 1:
                X = inputs[:, None]
            else:
                X = inputs
            # Normalizing the input
            mean_x = np.mean(X, axis = 0)
            std_x = np.std(X, axis = 0, keepdims = True)
            Xnorm = (X - mean_x)/std_x
            # Normalizing the outputs
            mean_y = np.mean(outputs)
            std_y = np.std(outputs)
            Ynorm = (outputs - mean_y)/std_y
            self.scaling= [[mean_x, std_x], [mean_y, std_y]]
            # Storing labels
            if input_labels == []:
                self.input_labels = ['x' + str(i) for i in range(Xnorm.shape[1])]
            elif  (len(input_labels) != Xnorm.shape[1]) or not(all(isinstance(s, str) for s in input_labels)):
                raise Exception('Invalid input for input_labels')
            else:
                self.input_labels = input_labels

            if output_label == None:
                self.output_label = 'y'
            elif not(isinstance(output_label,str)):
                raise Exception('Invalid input for output_label')
            else:
                self.output_label = output_label
            # Instantiating dictionary for storing sensitivity computations
            self.sensitivity_info = {}
            self.sensitivity_info['quotient_variances'] ={}
            self.sensitivity_info['quotient_variances']['full_expected_variance'] = None
            self.sensitivity_info['quotient_variances']['expected_marginal_variance'] = None
            self.sensitivity_info['main_effect_gp']={}
            self.sensitivity_info['interaction_effect_gp'] = {}

        BayesianGP.__init__(self, Xnorm, Ynorm, self.kernel_type, noise_level = noise_level, name = name)

        # Bounds needed for sensitivity analysis
        self.doe_bounds = np.zeros((Xnorm.shape[1],2))
        self.doe_bounds[:,0] = np.min(Xnorm, axis = 0)
        self.doe_bounds[:,1] = np.max(Xnorm, axis = 0)

        return

    def get_model_info(self):
        model_info = {}
        model_info['samples'] = copy.deepcopy(self.hyperpar_samples)
        model_info['kernel_type']= self.kernel_type
        model_info['noise_level'] = self.noise
        model_info['input_labels'] = self.input_labels
        model_info['output_label']= self.output_label
        model_info['Xnorm'] = self.Xtrain.numpy()
        model_info['Ynorm'] = self.Ytrain.numpy()
        model_info['scaling'] = self.scaling
        return model_info


    def run_mcmc(self,mcmc_samples, num_burnin_steps, num_adaptation_steps, adaptation_rate = 0.40, init_step_size = 0.01, num_leapfrog_steps = 3,thinning = 2):
        # Function used to perform the sampling for the posterior distributions of the hyperparameters
		# Inputs:
		#	mcmc_samples := number of samples to collect for the hyperparameters
		#	num_burnin_steps := number of samples to discard
		#  num_adaptation_steps := number of steps used to execute adaptive HMC
		# 	init_step_size :=  initial step_size for the HMC sampler
		# 	num_leapfrog_steps := number of leapfrog steps for the HMC sampler
		# thinning := number of mcmc samples to skip in between before storing
		# Outputs:
		#	hyp_samples= dictionary containing samples of the hyperparmeters
		#   acceptance_rate_ := acceptance rate of the sampling

        hyp_samples, average_acceptance_ratio = self.mcmc(mcmc_samples, num_burnin_steps,
                                                        num_adaptation_steps, adaptation_rate,
                                                        init_step_size, num_leapfrog_steps,thinning)


        self.hyperpar_samples['kernel_variance'] = hyp_samples[1].numpy()
        self.hyperpar_samples['kernel_inverse_lengthscales'] = hyp_samples[0].numpy()
        self.hyperpar_samples['gp_constant_mean_function'] = hyp_samples[-1].numpy()

        print('Average acceptance ratio: ', average_acceptance_ratio.numpy())

        return

    def plot_chains(self, display = True, save_plot = False, directory_path = None):
        # Function used to plot the chains from the  mcmc sampling
        # Inputs:
        #   directory_path:= directory where to save the plots. It defaults to the current directory if not specified
        #
        if len(self.hyperpar_samples) == 0:
            raise Exception('Hyperparameter samples must be generated or retrieved first.')

        nplots = self.dim_input +  2
        fig, axes = plt.subplots(nplots, 1, figsize=(20, 2.0*nplots),sharex=True)
        # plotting the samples for the kernel variance
        axes[0].plot(self.hyperpar_samples['kernel_variance'])
        title = 'kernel_variance_samples'
        axes[0].set_title(title)
        # plotting the samples for the constant mean function of the GP
        axes[1].plot(self.hyperpar_samples['gp_constant_mean_function'])
        title = 'gp_constant_mean_function_samples'
        axes[1].set_title(title)
        # plotting the samples for the inverse lengthscales
        for i in range(0,self.dim_input):
            axes[i+2].plot(self.hyperpar_samples['kernel_inverse_lengthscales'][:,i])
            title =  self.input_labels[i] + '_inverse_lengthscale_samples'
            axes[i+2].set_title(title)
        if save_plot:
            if directory_path == None:
                directory_path = os.getcwd()
            if not(os.path.isdir(directory_path)):
                raise Exception('Invalid directory path ', directory_path)
            figpath ='mcmc_chains.png'
            figpath = os.path.join(directory_path, figpath)
            plt.savefig(figpath)
        if display:
            plt.show()
        else:
            plt.close()
        return


    # def plot_sensitivity_variation(self, directory_path = None):
    #     # Function used to plot the sensitivty  varaiation boxplot
    #
    #     if self.n_inputs == 1:
    #         print('Not enough variables to perform sensitivity analysis.')
    #         return
    #     if len(self.hyperpar_samples) == 0:
    #         raise Exception('Hyperparameter samples must be generated or retrieved first.')
    #     if directory_path == None:
    #         directory_path = os.getcwd()
    #     if not(os.path.isdir(directory_path)):
    #         raise Exception('Invalid directory path ', directory_path)
    #
    #     beta_samples = self.hyperpar_samples['kernel_inverse_lengthscales']
    #     figpath = 'sensitivity_variation.png'
    #     figpath = os.path.join(directory_path, figpath)
    #     sensitivity.generateBetaBoxPlots(bounds=self.Rangenorm, beta_samples_list=[beta_samples], labels=self.labels, figpath = figpath)
    #     return


    def get_predictions(self, Xtest, fast = False):
        # Computes approximate values of the full posterior mean and variance of the Gaussian process
		# by using samples of the posterior distribution of the hyperparameters
		#Inputs:
		#	Xtest :=  N x D input array
		#	fast = Boolean specifying if we use just median values of the hyperparmeters
        #          or if all the samples are used.
		# Outputs:
		#	mean_pos, std_pos := the mean and standard deviation for the posterior Gaussian process (arrays of shape N)

        if len(self.hyperpar_samples) == 0:
            raise Exception('Hyperparameter samples must be generated or retrieved first.')

        mean_x, std_x = self.scaling[0]
        mean_y, std_y = self.scaling[1]

        if len(Xtest.shape) == 1:
            Xnew = Xtest[:, None]
        else:
            Xnew = Xtest
        Xtest_norm = (Xnew - mean_x)/std_x
        Xtest_norm = tf.convert_to_tensor(Xtest_norm, tf.float32)

        if fast:
            hyps = []
            beta_median = np.median(self.hyperpar_samples['kernel_inverse_lengthscales'], axis =0)
            beta_median = tf.convert_to_tensor(beta_median, tf.float32)
            hyps.append(beta_median)
            vark_median = np.median(self.hyperpar_samples['kernel_variance'], axis =0)
            vark_median = tf.convert_to_tensor(vark_median, tf.float32)
            hyps.append(vark_median)
            loc_median = np.median(self.hyperpar_samples['gp_constant_mean_function'], axis=0)
            loc_median = tf.convert_to_tensor(loc_median, tf.float32)
            hyps.append(loc_median)
            n_test = len(Xtest)
            mean_pos, var_pos = self.posteriorGP(Xtest_norm, n_test, hyps, False)
        else:
            hyp_samples = []
            beta_samples = tf.convert_to_tensor(self.hyperpar_samples['kernel_inverse_lengthscales'], tf.float32)
            hyp_samples.append(beta_samples)
            vark_samples = tf.convert_to_tensor(self.hyperpar_samples['kernel_variance'], tf.float32)
            hyp_samples.append(vark_samples)
            loc_samples = tf.convert_to_tensor(self.hyperpar_samples['gp_constant_mean_function'], tf.float32)
            hyp_samples.append(loc_samples)
            mean_pos, var_pos = self.predict(Xtest_norm, hyp_samples)

        mean_pos = mean_pos.numpy()[:,0]
        var_pos = var_pos.numpy()[:,0]
        std_pos = np.sqrt(var_pos)
        # Converting to the proper scale
        mean_pos = mean_pos*std_y + mean_y
        std_pos = std_pos*std_y

        return mean_pos, std_pos



    # # Sensitivity analysis
    # def maineffect_and_interaction(self, grid_points = 30, nx_samples = None, directory_path1 = None, directory_path2 = None, create_plot = True, batch_size=10):
    #     # Computes  and generate main_effect function plots
    #     # Inputs:
    #     #   grid_points:= the number of grid poinst for the plots
    #     #   nx_samples = the number of sample points for the Monte Carlo integration. Will default
    #     #       to a multiple of the number of variables if not provided
    #     #   directory_path1 :=  directory where to save the main effect plots if needed. Defaults to current directory
    #     #       if not specified
    #     #   directory_path2 :=  directory where to save the interaction surface plots if needed. Defaults to current directory
    #     #       if not specified
    #     #  create_plot := specifies if the plost should be generated are not
    #     # Outputs:
    #     #      main, interaction: = dictionaries containing values for the mean and interaction functions
    #
    #     # Main effect
    #     if self.n_inputs == 1:
    #         print('Not enough variables to perform sensitivity analysis.')
    #         return
    #     if len(self.hyperpar_samples) == 0:
    #         raise Exception('Hyperparameter samples must be generated or retrieved first.')
    #     if directory_path1 == None:
    #         directory_path1 = os.getcwd()
    #     if not(os.path.isdir(directory_path1)):
    #         raise Exception('Invalid directory path ', directory_path1)
    #     if directory_path2 == None:
    #         directory_path2 = os.getcwd()
    #     if not(os.path.isdir(directory_path2)):
    #         raise Exception('Invalid directory path ', directory_path2)
    #
    #     mean_y, std_y = self.scaling_output
    #     loc_samples = self.hyperpar_samples['gp_constant_mean_function']
    #     varm_samples = self.hyperpar_samples['kernel_variance']
    #     beta_samples = self.hyperpar_samples['kernel_inverse_lengthscales']
    #     hyperpar_samples =   [loc_samples, varm_samples, beta_samples]
    #     if nx_samples == None:
    #         nx_samples = 300*self.n_inputs
    #     selected_vars = np.array([i for i in range(self.n_inputs)])
    #
    #     ybase = sensitivity.allEffect(self.model, self.Rangenorm, nx_samples, hyperpar_samples)
    #     if self.n_inputs <= batch_size:
    #         y_main = sensitivity.mainEffect(self.model, self.Rangenorm, selected_vars, nx_samples, hyperpar_samples, grid_points)
    #     else:
    #         y_main = {}
    #         n_batches = self.n_inputs//batch_size
    #         vars_groups = np.array_split(selected_vars,n_batches)
    #         completed = 0
    #         for group in vars_groups:
    #             y_group = sensitivity.mainEffect(self.model, self.Rangenorm, group, nx_samples, hyperpar_samples, grid_points)
    #             completed += len(group)
    #             progress = 100.0*completed/self.n_inputs
    #             print("Main effect computation: {:.2f}% complete".format(progress))
    #             y_main.update(y_group)
    #
    #     z_mean = np.zeros((self.n_inputs, grid_points))
    #     z_std = np.zeros((self.n_inputs, grid_points))
    #     for i in range(self.n_inputs):
    #         key = tuple([i])
    #         z_mean[i,:] = y_main[key][:,0] - ybase[0][0,0]
    #         # The next 3 lines give an approximation of the standard deviation of the normalized main effect function E[Y|xi] - E[Y]
    #         lower_app = np.sqrt(np.abs(np.sqrt(y_main[key][:,1]) - np.sqrt(ybase[0][0,1])))
    #         upper_app = np.sqrt(y_main[key][:,1]) + np.sqrt(ybase[0][0,1])
    #         z_std[i,:] = (lower_app + upper_app)/2.0
    #     # Converting to the proper scale and plotting
    #     main = {}
    #     for i in range(self.n_inputs):
    #         y = z_mean[i,:]*std_y + mean_y
    #         y_std = z_std[i,:]*std_y
    #         x = np.linspace(self.Range[i,0], self.Range[i,1], grid_points)
    #         key = self.labels[i]
    #         main[key] = {}
    #         main[key]['inputs'] = x
    #         main[key]['output_mean']= y
    #         main[key]['output_std']= y_std
    #     if create_plot:
    #         print('Generating main effect plots.')
    #         if self.n_inputs <= 6:
    #             fig, axes = plt.subplots(nrows=1, ncols=self.n_inputs, sharey=True, figsize =(20,10))
    #             for i in range(self.n_inputs):
    #                 key = self.labels[i]
    #                 x = main[key]['inputs']
    #                 y = main[key]['output_mean']
    #                 y_std = main[key]['output_std']
    #                 axes[i].plot(x,y, label= self.labels[i])
    #                 axes[i].fill_between(x, y-2*y_std, y + 2*y_std, alpha = 0.2, color ='orange')
    #                 axes[i].grid()
    #                 axes[i].set_xlabel(key)
    #             axes[0].set_ylabel(self.output_label)
    #                 # axes[i].legend()
    #             title = 'main_effects'
    #             plt.title(title)
    #             figpath = title + '.png'
    #             figpath = os.path.join(directory_path1, figpath)
    #             plt.savefig(figpath)
    #             plt.close(fig)
    #         else:
    #             plot_rows = math.ceil(self.n_inputs/6)
    #             fig, axes = plt.subplots(nrows=plot_rows, ncols=6, sharey=True, figsize =(20,15))
    #             for i in range(self.n_inputs):
    #                 row_idx = i//6
    #                 col_idx = i%6
    #                 key = self.labels[i]
    #                 x = main[key]['inputs']
    #                 y = main[key]['output_mean']
    #                 y_std = main[key]['output_std']
    #                 axes[row_idx, col_idx].plot(x,y, label= self.labels[i])
    #                 axes[row_idx, col_idx].fill_between(x, y-2*y_std, y + 2*y_std, alpha = 0.2, color ='orange')
    #                 axes[row_idx, col_idx].grid()
    #                 # axes[row_idx, col_idx].legend()
    #                 axes[row_idx, col_idx].set_xlabel(key)
    #                 axes[row_idx, 0].set_ylabel(self.output_label)
    #             title = 'main_effects'
    #             plt.title(title)
    #             figpath = title + '.png'
    #             figpath = os.path.join(directory_path1, figpath)
    #             plt.savefig(figpath)
    #             plt.close(fig)
    #
    #     #---------------------------------------------------------------------
    #     # Interaction effect
    #     print("Starting interaction computations.")
    #     selected_pairs = []
    #     for i in range(self.n_inputs-1):
    #         for j in range(i+1,self.n_inputs):
    #             selected_pairs.append([i,j])
    #     selected_pairs = np.array(selected_pairs)
    #     n_pairs = len(selected_pairs)
    #     if n_pairs <= batch_size:
    #         y_int = sensitivity.mainInteraction(self.model, self.Rangenorm, selected_pairs, nx_samples, hyperpar_samples, grid_points)
    #     else:
    #         y_int = {}
    #         n_batches = n_pairs//batch_size
    #         pairs_groups = np.array_split(selected_pairs,n_batches,axis =0)
    #         completed = 0
    #         for group in pairs_groups:
    #             y_group = sensitivity.mainInteraction(self.model, self.Rangenorm, group, nx_samples, hyperpar_samples, grid_points)
    #             completed += len(group)
    #             progress = 100.0*completed/n_pairs
    #             print("Interaction effect computation: {:.2f}% complete".format(progress))
    #             y_int.update(y_group)
    #
    #     z_intmean = np.zeros((n_pairs, grid_points, grid_points))
    #     z_intstd = np.zeros((n_pairs, grid_points, grid_points))
    #     for k in  range(n_pairs):
    #         key = tuple(selected_pairs[k])
    #         y_slice = np.reshape(y_int[key],(grid_points,grid_points,2))
    #         j1, j2 = selected_pairs[k]
    #         key1 = tuple([j1])
    #         key2 = tuple([j2])
    #         v1 = y_main[key1][:,0]
    #         v2 = y_main[key2][:,0]
    #         p1, p2 = np.meshgrid(v1,v2)
    #         w1 = np.sqrt(y_main[key1][:,1])
    #         w2 = np.sqrt(y_main[key2][:,1])
    #         q1, q2 = np.meshgrid(w1,w2)
    #         z_intmean[k,:,:] = y_slice[:,:,0] - p1 - p2 +  ybase[0][0,0]
    #         upper_app = np.sqrt(y_slice[:,:,1]) + q1 + q2 + np.sqrt(ybase[0][0,1])
    #         lower_app = np.abs(np.sqrt(y_slice[:,:,1]) - q1 - q2 + np.sqrt(ybase[0][0,1]))
    #         z_intstd[k,:,:] = (upper_app + lower_app)/2.0
    #
    #     # Converting to the proper scale and storing
    #     interaction = {}
    #     for k in range(n_pairs):
    #         item = selected_pairs[k]
    #         j1, j2 = item
    #         x = np.linspace(self.Range[j1,0],self.Range[j1,1],grid_points)
    #         y = np.linspace(self.Range[j2,0],self.Range[j2,1],grid_points)
    #         Z = z_intmean[k,:,:]*std_y + mean_y
    #         Zstd = z_intstd[k,:,:]*std_y
    #         key = self.labels[j1] + '_&_' + self.labels[j2]
    #         X,  Y = np.meshgrid(x,y)
    #         interaction[key] = {}
    #         interaction[key]['input1'] = X
    #         interaction[key]['input2'] = Y
    #         interaction[key]['output_mean'] = Z
    #         interaction[key]['output_std'] = Zstd
    #
    #     if create_plot:
    #         print('Generating interaction surfaces plots.')
    #         # Bounds for the interaction surface plot
    #         zmin = np.min(z_intmean)*std_y + mean_y
    #         zmax = np.max(z_intmean)*std_y + mean_y
    #         minn = np.min(z_intstd)*std_y
    #         maxx = np.max(z_intstd)*std_y
    #
    #         for k in range(n_pairs):
    #             item = selected_pairs[k]
    #             j1, j2 = item
    #             key = self.labels[j1] + '_&_' + self.labels[j2]
    #             X = interaction[key]['input1']
    #             Y = interaction[key]['input2']
    #             Z = interaction[key]['output_mean']
    #             Zstd = interaction[key]['output_std']
    #             fig = plt.figure(figsize = (20,10))
    #             norm = mpl.colors.Normalize(minn, maxx)
    #             m = plt.cm.ScalarMappable(norm=norm, cmap='jet')
    #             m.set_array(Zstd)
    #             m.set_clim(minn, maxx)
    #             color_dimension = Zstd
    #             fcolors = m.to_rgba(color_dimension)
    #             ax = fig.gca(projection='3d')
    #             ax.plot_surface(Y, X, Z, rstride=1, cstride=1,facecolors= fcolors, shade = False)
    #             title = key
    #             ax.set_title(title)
    #             ax.set_xlabel(self.labels[j2])
    #             ax.set_ylabel(self.labels[j1])
    #             ax.set_zlabel(self.output_label)
    #             ax.set_zlim(zmin,zmax)
    #             plt.gca().invert_xaxis()
    #             plt.colorbar(m)
    #             figpath = title + '.png'
    #             figpath = os.path.join(directory_path2, figpath)
    #             plt.savefig(figpath)
    #             plt.close(fig)
    #
    #     return main, interaction
    #
    #
    #
    def execute_sobol_main_comp(self, desired_indices = [], subset_vars= [], max_order = 2, nx_samples = 5000, batch_size = 10, forced = True):
        # Executes quotient variances computations needed for sobol indices
        # This is the fundamental computation and can be performed in parallel
        # with respect to the indices
        # Inputs:
        #   desired_indices := list of sublist of variables where each sublist represents
        #                     a desired sobol index
        #   subset_vars     := subset of vars for which we want to compute sobol indices
        #                     (this is only considered when desired_indices is an empty list)
        #   max_order := maximum order of sobol indices to compute when using subset_vars
        #   nx_samples := number of samples used for the Monte Carlo integration
        #   forced    := execute computation if values were already computed

        if self.dim_input == 1:
            print('Not enough variables to perform sensitivity analysis.')
            return
        if len(self.hyperpar_samples) == 0:
            raise Exception('Hyperparameter samples must be generated or retrieved first.')

        if len(desired_indices)>0:
            desired_subsets = set([])
            for subset in desired_indices:
                desired_subsets = desired_subsets | set(labelling.generate_required_subset_list(subset_vars, self.input_labels))
            desired_subsets = list(desired_subsets)
        else:
            if len(subset_vars) == 0:
                S = [i for i in range(self.dim_input)]
            else:
                S = [self.input_labels.index(entry) for enrty in subset_vars]
                S = list(set(S))
                S.sort()
            n_s = len(S)
            if max_order >  self.dim_input:
                raise Exception('max_order cannot be greater than the number of variables in the given subset of variables')
            desired_subsets = labelling.powerset(S,1, max_order)

        print('Number of Sobol computations desired: ', len(desired_subsets))
        if forced:
            subsets_list_used = desired_subsets
        else:
            subsets_list_used = []
            for subset in desired_subsets:
                key = tuple(subset)
                if not(key in self.sensitivity_info['quotient_variances']):
                    subsets_list_used.append(subset)
        n_comp = len(subsets_list_used)
        print('Actual mumber of Sobol computations to be executed: ', n_comp)

        if n_comp > 0:
            hyps = []
            beta_median = np.median(self.hyperpar_samples['kernel_inverse_lengthscales'], axis =0)
            beta_median = tf.convert_to_tensor(beta_median, tf.float32)
            hyps.append(beta_median)
            vark_median = np.median(self.hyperpar_samples['kernel_variance'], axis =0)
            vark_median = tf.convert_to_tensor(vark_median, tf.float32)
            hyps.append(vark_median)
            loc_median = np.median(self.hyperpar_samples['gp_constant_mean_function'], axis=0)
            loc_median = tf.convert_to_tensor(loc_median, tf.float32)
            hyps.append(loc_median)
            grid_points = nx_samples//2
            # ybase = sensitivity.allEffect(self.model, self.Rangenorm, nx_samples, hyperpar_samples)
            # ey_square = sensitivity.direct_samples(self.model, self.Rangenorm, nx_samples, hyperpar_samples)
            if n_comp <= batch_size:
                Q = sensitivity.get_marginal_posteriorGP(self, hyps, self.doe_bounds, subsets_list_used, grid_points, nx_samples, reduced = True)
            else:
                Q = {}
                completed = 0
                n_groups = math.ceil(n_comp/batch_size)
                for i in range(n_groups):
                    group = subsets_list_used[i*batch_size:(i+1)*batch_size]
                    Q_group = sensitivity.get_marginal_posteriorGP(self, hyps, self.doe_bounds, group , grid_points, nx_samples, reduced = True)
                    completed += len(group)
                    progress = 100.0*completed/n_comp
                    print("Sobol indices computation: {:.2f}% complete".format(progress))
                    Q.update(Q_group)

            if forced or (self.sensitivity_info['quotient_variances']['full_expected_variance'] == None):
                self.sensitivity_info['quotient_variances']['full_expected_variance']= sensitivity.get_full_posteriorGP(self, hyps, self.doe_bounds, nx_samples, reduced = True)
                self.sensitivity_info['quotient_variances']['expected_marginal_variance']= sensitivity.get_fullmarginal_posteriorGP(self, hyps, self.doe_bounds, nx_samples, reduced = True)
            e1 = self.sensitivity_info['quotient_variances']['full_expected_variance']
            e0 = self.sensitivity_info['quotient_variances']['expected_marginal_variance']

            for key in Q:
                self.sensitivity_info['quotient_variances'][key] = (Q[key] -e0)/(e1-e0)
        return


    def get_Sobol_indices(self):
        # returns a dictionary containing the sobol indices that are already available
        keys = list(self.sensitivity_info['quotient_variances'])
        keys.remove('full_expected_variance')
        keys.remove('expected_marginal_variance')
        keys.sort(key=len)
        n_keys = len(keys)
        Sobol = {}
        for i in range(n_keys):
            key = keys[i]
            sensitivity.compute_Sobol(key, self.sensitivity_info['quotient_variances'], Sobol)
        Sobol_out = {}
        for key in Sobol:
            label = labelling.generate_label(key, self.input_labels)
            Sobol_out[label] = Sobol[key]
        return Sobol_out


        # all_labels = list(Sobol.keys())
        # si_all = list(Sobol.values())
        #
        # # plotting
        # si_all = np.array(si_all)
        # order = np.argsort(-si_all)
        # n_selected = min(40, len(si_all))
        # selected = order[:n_selected] # taking the top 40 values to plot
        # y_pos = np.arange(n_selected)
        # if create_plot:
        #     print('Generating Sobol indices barplot.')
        #     plt.figure(figsize =(12,12))
        #     # Create bars
        #     plt.barh(y_pos, si_all[selected])
        #     new_labels = [all_labels[selected[i]] for i in range(n_selected)]
        #     title = 'top_sobol_indices'
        #     plt.title(title)
        #     # Create names on the y-axis
        #     plt.yticks(y_pos, new_labels)
        #     plt.xlabel('sobol_index')
        #     figpath = title + '.png'
        #     figpath = os.path.join(directory_path, figpath)
        #     plt.savefig(figpath)
        #     plt.close()
        #
        # return Sobol
    #
    # def total_sobol_indices(self, nx_samples = None, directory_path = None, create_plot = True, batch_size=10):
    #     # Computes total sobol indices and generate bar plot.
    #     # Inputs:
    #     #   nx_samples = the number of sample points for the Monte Carlo integration. Will default
    #     #           to a multiple of the number of variables if not provided
    #     #  directory_path :=  directory where to save the Sobol barplot if needed. Defaults to current directory
    #     #       if not specified
    #     #    create_plot := specifies if the total Sobol barplot should be generated are not
    #     # Outputs:
    #     #   Sobol_total := dictionary containining containing the total Sobol indices values
    #
    #
    #     if self.n_inputs == 1:
    #         print('Not enough variables to perform sensitivity analysis.')
    #         return
    #     if len(self.hyperpar_samples) == 0:
    #         raise Exception('Hyperparameter samples must be generated or retrieved first.')
    #     if directory_path == None:
    #         directory_path = os.getcwd()
    #     if not(os.path.isdir(directory_path)):
    #         raise Exception('Invalid directory path ', directory_path)
    #
    #
    #     loc_samples = self.hyperpar_samples['gp_constant_mean_function']
    #     varm_samples = self.hyperpar_samples['kernel_variance']
    #     beta_samples = self.hyperpar_samples['kernel_inverse_lengthscales']
    #     hyperpar_samples =   [loc_samples, varm_samples, beta_samples]
    #     if nx_samples == None:
    #         nx_samples = 300*self.n_inputs
    #     selected_vars = np.array([i for i in range(self.n_inputs)])
    #
    #     ybase = sensitivity.allEffect(self.model, self.Rangenorm, nx_samples, hyperpar_samples)
    #     ey_square = sensitivity.direct_samples(self.model, self.Rangenorm, nx_samples, hyperpar_samples)
    #     if self.n_inputs  <= batch_size:
    #         y_remaining  = sensitivity.compute_remaining_effect(self.model, self.Rangenorm, selected_vars, nx_samples, hyperpar_samples)
    #     else:
    #         y_remaining = {}
    #         n_batches = self.n_inputs//batch_size
    #         vars_groups = np.array_split(selected_vars,n_batches)
    #         completed = 0
    #         for group in vars_groups:
    #             y_group = sensitivity.compute_remaining_effect(self.model, self.Rangenorm, group, nx_samples, hyperpar_samples)
    #             completed += len(group)
    #             progress = 100.0*completed/self.n_inputs
    #             print("Total Sobol indices computation: {:.2f}% complete".format(progress))
    #             y_remaining.update(y_group)
    #
    #     e1 = np.mean(ey_square[:,1] + np.square(ey_square[:,0]))
    #     e2 = ybase[0][0,1] + np.square(ybase[0][0,0])
    #     si_remaining  = np.zeros(self.n_inputs)
    #     for i in range(self.n_inputs):
    #         key = tuple([i])
    #         si_remaining[i] = np.mean(y_remaining[key][:,1] + np.square(y_remaining[key][:,0]))
    #     si_remaining  = (si_remaining -e2)/(e1-e2)
    #     si_remaining = np.maximum(si_remaining,0)
    #     si_total = 1 - si_remaining
    #     si_total = np.maximum(si_total,0)
    #
    #     if create_plot:
    #         print('Generating total Sobol indices barplot.')
    #         #  generating the plot
    #         order = np.argsort(-si_total)
    #         n_selected = min(40, len(si_total))
    #         selected = order[:n_selected] # taking the top 40 values to plot
    #         y_pos = np.arange(n_selected)
    #         plt.figure(figsize =(12,12))
    #         # Create bars
    #         plt.barh(y_pos, si_total[selected])
    #         new_labels = [self.labels[selected[i]] for i in range(n_selected)]
    #         title = 'top_total_sobol_indices'
    #         plt.title(title)
    #         # Create names on the y-axis
    #         plt.yticks(y_pos, new_labels)
    #         plt.xlabel('total_sobol_index')
    #         figpath = title + '.png'
    #         figpath = os.path.join(directory_path, figpath)
    #         plt.savefig(figpath)
    #         plt.close()
    #
    #     Sobol_total = {}
    #     for i in range(self.n_inputs):
    #         l = self.labels[i]
    #         Sobol_total[l] = si_total[i]
    #
    #     return Sobol_total
    #
    #
    # def group_sobol_indices(self, max_order = 2, partition = None, S = None, nx_samples = None, directory_path = None, create_plot = True, batch_size=10):
    #     # Computes group sobol indices and generate bar plot.
    #     # Inputs:
    #     #   partition := list fo lists specifying the partition of the variables
    #     #   S := dictionary containing previously computed Sobol indices. The computation of
    #     #      Sobol indices is a recursive computation
    #     #   max_order := maximum order of sobol indices to compute
    #     #   nx_samples = the number of sample points for the Monte Carlo integration. Will default
    #     #           to a multiple of the number of variables if not provided
    #     #  directory_path :=  directory where to save the Sobol barplot if needed. Defaults to current directory
    #     #       if not specified
    #     #  create_plot := specifies if the Sobol barplot should be generated are not
    #     # Outputs:
    #     #       Sobol := dictionary consistsing of two main keys:
    #     #               'mapping':= dictionary specifying the mapping between groups and variable indices
    #     #               'results':= dictionary containing the Sobol inidces values
    #     #       label_mapping := dictionary specifying the mapping between group labels and variable labels
    #
    #     if S == None:
    #         # generating the group mapping
    #         if partition == None:
    #             raise ValueError('specify a partition of the labels or provide previoulsy computed group Sobol indices dictionary')
    #         groups_map = sensitivity.create_groups(partition, self.labels)
    #     else:
    #         groups_map = S['mapping']
    #     n_group = len(groups_map)
    #
    #     if max_order >  n_group:
    #         raise Exception('max_order cannot be greater than the number of groups')
    #     if n_group == 1:
    #         print('Not enough groups to perform sensitivity analysis.')
    #         return
    #     if len(self.hyperpar_samples) == 0:
    #         raise Exception('Hyperparameter samples must be generated or retrieved first.')
    #     if directory_path == None:
    #         directory_path = os.getcwd()
    #     if not(os.path.isdir(directory_path)):
    #         raise Exception('Invalid directory path ', directory_path)
    #
    #     loc_samples = self.hyperpar_samples['gp_constant_mean_function']
    #     varm_samples = self.hyperpar_samples['kernel_variance']
    #     beta_samples = self.hyperpar_samples['kernel_inverse_lengthscales']
    #     hyperpar_samples =   [loc_samples, varm_samples, beta_samples]
    #     if nx_samples == None:
    #         nx_samples = 300*self.n_inputs
    #     selected_groups = [i for i in range(n_group)]
    #
    #     initial_list  = sensitivity.powerset(selected_groups,1, max_order)
    #     subsets_of_groups  = []
    #     if S != None:
    #         print('Initial number of Sobol computations: ', len(initial_list))
    #         try:
    #             for item in initial_list:
    #                 l = sensitivity.generate_group_label(item)
    #                 if not (l in S['results'].keys()):
    #                     subsets_of_groups.append(item)
    #             print('New number of Sobol computations: ', len(subsets_of_groups))
    #         except Exception as e:
    #             traceback.print_exc()
    #             print('Invalid Sobol indices dictionary')
    #     else:
    #         subsets_of_groups  = initial_list
    #     variable_subsets = []
    #     for entry in subsets_of_groups:
    #         variable_subsets.append(sensitivity.get_variable_indices_list(entry, groups_map))
    #
    #     n_subset = len(subsets_of_groups)
    #     if n_subset > 0:
    #         ybase = sensitivity.allEffect(self.model, self.Rangenorm, nx_samples, hyperpar_samples)
    #         ey_square = sensitivity.direct_samples(self.model, self.Rangenorm, nx_samples, hyperpar_samples)
    #         if n_subset <= batch_size:
    #             y_higher_order = sensitivity.mainHigherOrder(self.model, self.Rangenorm, variable_subsets, nx_samples, hyperpar_samples)
    #         else:
    #             y_higher_order = {}
    #             completed = 0
    #             n_batches = math.ceil(n_subset/batch_size)
    #             for i in range(n_batches):
    #                 batch = variable_subsets[i*batch_size:(i+1)*batch_size]
    #                 y_batch = sensitivity.mainHigherOrder(self.model, self.Rangenorm, batch, nx_samples, hyperpar_samples)
    #                 completed += len(batch)
    #                 progress = 100.0*completed/n_subset
    #                 print("Sobol indices computation: {:.2f}% complete".format(progress))
    #                 y_higher_order.update(y_batch)
    #
    #         e1 = np.mean(ey_square[:,1] + np.square(ey_square[:,0]))
    #         e2 = ybase[0][0,1] + np.square(ybase[0][0,0])
    #         y_higher_order_group = {}
    #         for entry in subsets_of_groups:
    #             subset = sensitivity.get_variable_indices_list(entry, groups_map)
    #             k_group = tuple(entry)
    #             k_subset = tuple(subset)
    #             y_higher_order_group[k_group] = y_higher_order[k_subset]
    #         quotient_variances = {}
    #         for entry in subsets_of_groups:
    #             k_group = tuple(entry)
    #             quotient_variances[k_group] = np.mean(y_higher_order_group[k_group][:,1] + np.square(y_higher_order_group[k_group][:,0]))
    #             quotient_variances[k_group] = (quotient_variances[k_group] - e2)/(e1-e2)
    #
    #     if S != None:
    #         Sobol = S
    #     else:
    #         Sobol = {}
    #         Sobol['mapping']= groups_map
    #         Sobol['results'] = {}
    #     for i in range(n_subset):
    #         key = tuple(subsets_of_groups[i])
    #         sensitivity.compute_group_Sobol(Sobol['results'], quotient_variances, key)
    #
    #     all_labels = list(Sobol['results'].keys())
    #     si_all = list(Sobol['results'].values())
    #
    #     # plotting
    #     si_all = np.array(si_all)
    #     order = np.argsort(-si_all)
    #     n_selected = min(40, len(si_all))
    #     selected = order[:n_selected] # taking the top 40 values to plot
    #     y_pos = np.arange(n_selected)
    #     if create_plot:
    #         print('Generating group Sobol indices barplot.')
    #         plt.figure(figsize =(12,12))
    #         # Create bars
    #         plt.barh(y_pos, si_all[selected])
    #         new_labels = [all_labels[selected[i]] for i in range(n_selected)]
    #         title = 'top_group_sobol_indices'
    #         plt.title(title)
    #         # Create names on the y-axis
    #         plt.yticks(y_pos, new_labels)
    #         plt.xlabel('sobol_index')
    #         figpath = title + '.png'
    #         figpath = os.path.join(directory_path, figpath)
    #         plt.savefig(figpath)
    #         plt.close()
    #     # generate label_mapping
    #     label_mapping = {}
    #     for k in groups_map:
    #         label_mapping[k] = [self.labels[i] for i  in groups_map[k]]
    #
    #     return Sobol, label_mapping
