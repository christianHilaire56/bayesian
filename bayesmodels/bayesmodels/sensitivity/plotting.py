import numpy as np
import matplotlib.pyplot as plt




def generateBetaBoxPlots(bounds, beta_samples_list, labels, figpath = None, calibration= False, type = 'simulator'):
	# Generate Box plots for a metric defined in terms of the inverse lengthscale
    if calibration:
        betasx_samples, betaspar_samples, betad_samples = beta_samples_list
        # For the simulator
        Range = (bounds[:,1] - bounds[:,0])[:,None]
        n_inputs = betasx_samples.shape[1]
        n_pars = betaspar_samples.shape[1]
        if type == 'simulator':
            metric_x = 1 - np.exp(-np.square(betasx_samples*(Range[:n_inputs,0]))/8.0)
            metric_par = 1 - np.exp(-np.square(betaspar_samples*(Range[n_inputs:,0]))/8.0)
            data_to_plot = []
            for i in range(n_inputs):
                data_to_plot.append(metric_x[:,i])
            for i in range(n_pars):
        	    data_to_plot.append(metric_par[:,i])
            plt.figure(figsize=(20, 10))
            # Create the boxplot
            plt.boxplot(data_to_plot,  showfliers=False)
            locs, _ = plt.xticks()
            plt.xticks(locs, labels)
            plt.ylim((0,1))
            plt.ylabel('sensitivity')
            plt.title('Simulator model')
            if figpath:
                plt.savefig(figpath)
                plt.close()
        if type == 'discrepancy':
            # For the discrepancy
            metric_x = 1 - np.exp(-np.square(betad_samples*(Range[:n_inputs,0]))/8.0)
            data_to_plot = []
            for i in range(n_inputs):
                data_to_plot.append(metric_x[:,i])
            plt.figure(figsize=(20, 10))
            # Create the boxplot
            plt.boxplot(data_to_plot,  showfliers=False)
            plt.xticks(locs, labels[:n_inputs])
            plt.title('Discrepancy model')
            plt.ylim((0,1))
            plt.ylabel('sensitivity')
            if figpath:
                plt.savefig(figpath)
                plt.close()
    else:
        beta_samples = beta_samples_list[0]
        Range = (bounds[:,1] - bounds[:,0])[:,None]
        n_inputs = beta_samples.shape[1]
        metric_x = 1 - np.exp(-np.square(beta_samples*(Range[:n_inputs,0]))/8.0)
        data_to_plot = []
        for i in range(n_inputs):
            data_to_plot.append(metric_x[:,i])

        plt.figure(figsize=(20, 10))
        # Create the boxplot
        plt.boxplot(data_to_plot,  showfliers=False)
        locs, _ = plt.xticks()
        plt.xticks(locs, labels)
        plt.ylim((0,1))
        plt.ylabel('sensitivity')
        if figpath:
            plt.savefig(figpath)
            plt.close()
    return
