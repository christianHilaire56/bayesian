import numpy as np
from pyDOE import lhs
import copy

#-------------------------------------------------------------------------------
#---- Functions for generating samples used in the sensitivity analysis --------
#-------------------------------------------------------------------------------
def affineTransform(X, b, a):
    return X*a + b

def uniform_to_triangular(u):
    # function to convert a standard uniform random variable to a random variable
    # with a triangular distribution
    if u < 0.5:
        return (2*u)**0.5 -1.0
    else:
        return 1.0 - (2*(1-u))**0.5

# vectorizing the function
vuniform_to_triangular = np.vectorize(uniform_to_triangular)

def generate_marginal_samples(bounds, subsets_list, grid_points, nx_samples, fixing_subset = True):
    # Function used to generate the samples needed to compute the marginal
    # posterior GP
    #  Inputs:
    #    bounds := array specifying the bounds of the uniform distribution of each
    #           variable
    #   subsets_list := list of subsets of indices. Each subset specifies a subvector
    #                 Xp (or its complement) for which we want to compute the marginal GP
    #   grid_points := number of grid points for each subvector Xp
    #   nx_samples  := number of samples for each grid point used to compute E[Y|Xp]
    # Outputs:
    #     sampling_dict:= dictionary containing samples used for computing the
    #            marginal GPs
    n_var = bounds.shape[0]
    n_subset = len(subsets_list)
    a = bounds[:,1] -bounds[:,0]
    a = a[None,:]
    b = bounds[:,0][None,:]
    a_aug = a[np.newaxis, :]
    b_aug = b[np.newaxis, :]
    sampling_dict ={}
    vars = set([i for i in range(n_var)])
    # Generating list of fixed variables and list of marginalized variables
    if fixing_subset:
        subsets_fixed = copy.deepcopy(subsets_list)
        subsets_marg = []
        for idx in range(n_subset):
            subset = subsets_fixed[idx]
            vars_left = list(vars -set(subset))
            vars_left.sort()
            subsets_marg.append(vars_left)
    else:
        subsets_marg = copy.deepcopy(subsets_list)
        subsets_fixed = []
        for idx in range(n_subset):
            subset = subsets_marg[idx]
            vars_left = list(vars -set(subset))
            vars_left.sort()
            subsets_fixed.append(vars_left)
    # Generating the samples needed for the sensitivity computation
    for idx in range(n_subset):
        key = tuple(subsets_list[idx])
        fixed = subsets_fixed[idx]
        marg = subsets_marg[idx]
        size_fixed = len(fixed)
        size_marg = len(marg)
        M_fixed = lhs(size_fixed, samples = grid_points)
        M_marg = lhs(size_marg, samples = nx_samples)
        M_marg = np.tile(M_marg[np.newaxis,:,:],[grid_points,1,1])
        for jdx in range(size_fixed):
            j = fixed[jdx]
            points = M_fixed[:,jdx]
            M_marg = np.insert(M_marg,j, points[:, None], axis = -1)
        sampling_dict[key] ={}
        sampling_dict[key]['fixed_indices_list'] = fixed[:]
        sampling_dict[key]['marg_indices_list'] = marg[:]
        sampling_dict[key]['X_sampling'] = affineTransform(M_marg,b_aug, a_aug)
        sampling_dict[key]['X_grid'] = affineTransform(M_fixed, b[:,fixed], a[:, fixed])
        diff_samples = lhs(size_marg,nx_samples)
        if diff_samples.shape[1] > 0:
            diff_samples = vuniform_to_triangular(diff_samples)
        sampling_dict[key]['diff_samples'] = diff_samples*a[:,marg]
    return sampling_dict


def generate_main_effect_samples(bounds, selected_vars, num_per_var, nx_samples):
        # Function used to generate the samples needed to compute the marginal
        # posterior GP for the special case where we are just fixing onve variable
        #  Inputs:
        #    bounds := array specifying the bounds of the uniform distribution of each
        #           variable
        #   subsets_list := list of variables that will be fixed
        #   num__per_var := number of grid points for each variable fixed
        #   nx_samples  := number of samples for each grid point used to compute E[Y|Xp]
        # Outputs:
        #     sampling_dict:= dictionary containing samples used for computing the
        #            marginal GPs
        n_var = bounds.shape[0]
        n_selected = len(selected_vars)
        a = bounds[:,1] -bounds[:,0]
        a = a[None,:]
        b = bounds[:,0][None,:]
        a_aug = a[np.newaxis, :]
        b_aug = b[np.newaxis, :]
        grid_points = num_per_var
        points = np.linspace(0,1,grid_points)
        points = points[:, None]
        sampling_dict = {}
        vars = [i for i in range(n_var)]
        for idx in range(n_selected):
            j = selected_vars[idx]
            key  = tuple([j])
            M_marg = lhs(n_var-1, samples = nx_samples)
            diff_samples = lhs(n_var-1, nx_samples)
            diff_samples = vuniform_to_triangular(diff_samples)
            M_marg = np.tile(M_marg[np.newaxis,:,:],[grid_points,1,1])
            M_marg = np.insert(M_marg,j, points, axis = -1)
            sampling_dict[key] ={}
            vars_left = vars[:]
            vars_left.remove(j)
            sampling_dict[key]['fixed_indices_list'] = [j]
            sampling_dict[key]['marg_indices_list'] = vars_left
            sampling_dict[key]['X_sampling'] = affineTransform(M_marg,b_aug, a_aug)
            sampling_dict[key]['X_grid'] = affineTransform(points, b[:,j], a[:,j])
            sampling_dict[key]['diff_samples'] = diff_samples*a[:,vars_left]
        return sampling_dict


def generate_interaction_effect_samples(bounds, selected_pairs, num_per_var, nx_samples):
        # Function used to generate the samples needed to compute the marginal
        # posterior GP for the special case where we are just fixing onve variable
        #  Inputs:
        #    bounds := array specifying the bounds of the uniform distribution of each
        #           variable
        #   selected_pairs := list of sorted pairs of indices that are fixed
        #   num__per_var := number of points selected per variable when constructing the grid.
        #                 we will then have a total of (num_per_var)**2 grid points for
        #                 each interaction surface
        #   nx_samples  := number of samples for each grid point used to compute E[Y|Xp]
        # Outputs:
        #     sampling_dict:= dictionary containing samples used for computing the
        #            marginal GPs
        n_var = bounds.shape[0]
        n_selected = len(selected_pairs)
        a = bounds[:,1] -bounds[:,0]
        a = a[None,:]
        b = bounds[:,0][None,:]
        a_aug = a[np.newaxis, :]
        b_aug = b[np.newaxis, :]
        grid_points = num_per_var**2
        points = np.linspace(0,1,num_per_var)
        p1, p2 = np.meshgrid(points,points)
        q1 = p1.ravel()[:,None]
        q2 = p2.ravel()[:,None]
        sampling_dict = {}
        vars = [i for i in range(n_var)]
        for idx in range(n_selected):
            j1, j2 = selected_pairs[idx]
            key  = tuple([j])
            M_marg = lhs(n_var-2, samples = nx_samples)
            diff_samples = lhs(n_var-2, nx_samples)
            diff_samples = vuniform_to_triangular(diff_samples)
            M_marg = np.tile(M_marg[np.newaxis,:,:],[grid_points,1,1])
            M_marg = np.insert(M_marg,j1, q1, axis = -1)
            M_marg = np.insert(M_marg,j2, q2, axis = -1)
            sampling_dict[key] ={}
            vars_left = vars[:]
            vars_left.remove(j1)
            vars_left.remove(j2)
            sampling_dict[key]['fixed_indices_list'] = [j1,j2]
            sampling_dict[key]['marg_indices_list'] = vars_left
            sampling_dict[key]['X_sampling'] = affineTransform(M_marg,b_aug, a_aug)
            sampling_dict[key]['X_grid'] = affineTransform(np.concatenate([q1,q2], axis=0), b[:,[j1,j2]], a[:,[j1,j2]])
            sampling_dict[key]['diff_samples'] = diff_samples*a[:,vars_left]
        return sampling_dict



def generate_full_marginalization(bounds, nx_samples):
    n_var = bounds.shape[0]
    a = bounds[:,1] -bounds[:,0]
    a = a[None,:]
    b = bounds[:,0][None,:]
    a_aug = a[np.newaxis, :]
    b_aug = b[np.newaxis, :]
    sampling_dict ={}
    key= 'full_marg'
    sampling_dict[key] = {}
    vars = [i for i in range(n_var)]
    M_marg = lhs(n_var, samples = nx_samples)
    sampling_dict[key]['fixed_indices_list'] = []
    sampling_dict[key]['marg_indices_list'] = vars
    sampling_dict[key]['X_sampling'] = affineTransform(M_marg[np.newaxis,:,:], b_aug, a_aug)
    sampling_dict[key]['X_grid'] =[]
    diff_samples = lhs(n_var,nx_samples)
    sampling_dict[key]['diff_samples'] = diff_samples*a
    return sampling_dict




def generate_full_samples(bounds, nx_samples):
    # function used to generate full samples for computing the posterior GP
    n_var = bounds.shape[0]
    a = bounds[:,1] -bounds[:,0]
    a = a[None,:]
    b = bounds[:,0][None,:]

    M = lhs(n_var, samples = nx_samples)
    Xsamples = affineTransform(M, b, a)
    return Xsamples
