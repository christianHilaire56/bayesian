import numpy as np
import time
from .  import sampling
from . import labelling
#-------------------------------------------------------------------------------
#---- Functions for performing sensitivity analysis ----------------------------

#-------------------------------------------------------------------------------


#------------ References -------------------------------------------------------
# - [Rasmussen] "Gaussian process for Machine Learning", C. E. Rasmussen and C. K. I. Williams, MIT press
# - [Farah]   "Bayesian Inference for Sensitivity Analysis of Computer Simulators, with an Application to
#   Radiative Transfer Models",
# - [Janon] " Asynptotoc normality and efficiency of two Sobol index estimators",  A. Janon,
# T. Klein, A. Lagnoux-Renaudie, M. Nodet and C. Prieur, https://hal.inria.fr/hal-00665048v2
# - [Higdon]   "Combining Field Data and Computer Simulation for Calibration and Prediction",
#
#-------------------------------------------------------------------------------


def get_marginal_posteriorGP(model, hyps, bounds, subsets_list, grid_points, nx_samples, reduced = True):
    # Generate dictionary containing required samples
    sampling_dict = sampling.generate_marginal_samples(bounds, subsets_list, grid_points, nx_samples)
    return model.compute_marginal_posteriorGP(sampling_dict, hyps, reduced)

def get_fullmarginal_posteriorGP(model, hyps, bounds,nx_samples, reduced = True):
    sampling_dict = sampling.generate_full_marginalization(bounds, nx_samples)
    return  model.compute_marginal_posteriorGP(sampling_dict, hyps, reduced)['full_marg']


def get_full_posteriorGP(model, hyps, bounds, nx_samples, reduced = True):
    Xsamples = sampling.generate_full_samples(bounds, nx_samples)
    return model.compute_full_posteriorGP(Xsamples, hyps, reduced)


def get_maineffectGP(model, hyps, bounds, selected_vars, num_per_var, nx_samples):
    sampling_dict = sampling.generate_main_effect_samples(bounds, selected_vars, num_per_var, nx_samples)
    return model.compute_marginal_posteriorGP(sampling_dict, hyps, False)


def get_maininteractionGP(model, hyps, bounds, selected_pairs, num_per_var, nx_samples):
    sampling_dict = sampling.generate_main_effect_samples(bounds, selected_pairs, num_per_var, nx_samples)
    return model.compute_marginal_posteriorGP(sampling_dict, hyps, False)


def compute_Sobol(key, Q, S):
    # function used to compute internal sobol indices recursively given a dictionary
    # containing the values for the quotient variances
    # Inputs:=
    #   key:= internal key represented in terms of a tuple of indices
    #   Q  := dictionary containing the quotient variances
    #   S   :=  dictionary for storing the sobol indices
    S[key] = Q[key]
    if len(key) > 1:
        subsets = labelling.powerset(list(key),1,len(key)-1)
        for item in subsets:
            S[key] -= S[tuple(item)]
    S[key] = np.maximum(S[key],0)
    return

#
#
# def compute_group_Sobol(S,Q,key):
#     # function used to compute the sobol indices recursively given a dictionary
#     # Q containing the values for the quotient variances
#     l = generate_group_label(list(key))
#     S[l] = Q[key]
#     if len(key) > 1:
#         subsets = powerset(list(key),1,len(key)-1)
#         for item in subsets:
#             l_item = generate_group_label(item)
#             S[l] -= S[l_item]
#     S[l] = np.maximum(S[l],0)
#     return
