import numpy as np
from itertools import chain, combinations
import copy

def powerset(S,min_size, max_size):
    # function used to generate subsets of a set
    # Inputs:
    #   S := set ( of indices )
    #   min_size = minimum size of subsets allowed
    #   max_size = maximum size of subsets allowed
    # Output:
    #    out := list of subsets of S.
    p_set = list(chain.from_iterable(combinations(S,n) for n in range(min_size,max_size+1)))
    out = []
    for item in p_set:
        y = list(item)
        y.sort()
        out.append(y)
    return out

def generate_label(key, var_names):
    # function used to generate labels in terms of the variable names
    # Input:
    #   key := internal key represented by a tuple of indices
    #   var_names := list containing the variable names
    if len(key) > 1:
        target = [var_names[idx] for idx in key]
        return " || ".join(target)
    else:
        return var_names[key[0]]


def generate_required_subset_list(subset_vars, var_names):
    # function used to create the necessary subset list for executing
    # sensitivity computations for a subset of variables
    subset = []
    n_sub = len(subset_vars)
    for entry in subset_vars:
        if entry in var_names:
            subset.append(var_names.index(entry))
        else:
            raise ValueError('Invalid variable name %r in subset %r' %(entry, subset_vars))
    subset.sort()
    return powerset(subset, 1, n_sub)


def create_groups(partition, all_labels):
    # Inputs:
    #   partition = list of lists representing a partition of labels
    #   all_labels = list of labels
    # Output:
    #   groups_map:= dictionary where each key gives a list containing the variable indices for a group
    # Checking that the  partition is valid
    combined = []
    for entry in partition:
        combined +=entry
    cond1 = len(combined) == len(all_labels)
    cond2 = set(combined) == set(all_labels)
    if not(cond1 & cond2):
        raise ValueError('Partition is not valid.')
    groups_map = {}
    n_g = len(partition)
    for i in range(n_g):
        groups_map[i] = []
        for item in partition[i]:
            idx = all_labels.index(item)
            groups_map[i].append(idx)
        groups_map[i].sort()
    return groups_map

def generate_group_label(group_subset):
    # function used to generate group labels for group sobol indices
    if len(group_subset) > 1:
        target = ['G' + str(i) for i in group_subset]
        return " || ".join(target)
    else:
        return 'G' + str(group_subset[0])

def get_variable_indices_list(group_subset, groups_map):
    # function use to retrieve all the variable indices for a subset of groups given the group mapping
    idx_list = []
    for i in group_subset:
        idx_list += groups_map[i]
    idx_list.sort()
    return idx_list
