from abc import ABC, abstractmethod
import tensorflow as tf


class CoreModel(ABC):
    def __init__(self, name= None):
        # Inputs:
        #   name:= string specifying a name for the model"
        self.name = name
        super().__init__()
        return

    @abstractmethod
    def get_batch_and_event_shape(self):
		# function that outputs a dictionary specifying the batch and event shape for
		# the prior distribution of each hyperparameter
        pass

    @abstractmethod
    def get_initial_state(self):
		# function that outputs a dictionary specifying the initial state for
		# each hyperparameter
        pass


    @abstractmethod
    def update_prior(self, hyp_name, new_prior):
		# function used to update the prior distribution of a hyperparameter
		# Inputs
		#  hyp_name := name of the hyperparameter in the model
		#  new_prior := new prior distribution for the hyperparameter
        pass

    @abstractmethod
    def update_initial_state(self, hyp_name, new_initial_state):
		# function used to update the initial_state of a hyperparameter
		# Inputs
		#  hyp_name := name of the hyperparameter in the model
		#  new_initial_state := new initial_state for the hyperparameter
        pass

    @abstractmethod
    def joint_log_prob(self, *hyps):
        # function for computing the joint log probability of a model given
        # values for the hyperparameters of the model
        pass

    @abstractmethod
    def mcmc(self, mcmc_samples, num_burnin_steps, num_adaptation_steps, adaptation_rate, init_step_size,
              num_leapfrog_steps = 3, thinning = 2):
        # function for setting up the MCMC sampling for the posterior
        # distributions of the hyperparameters
        pass

    @abstractmethod
    def predict(self, X, hyp_samples):
        pass
