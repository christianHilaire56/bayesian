import tensorflow as tf
import numpy as np
import functools
import math
from tensorflow_probability.python.mcmc import sample_chain, HamiltonianMonteCarlo
from tensorflow_probability.python.mcmc import  TransformedTransitionKernel, SimpleStepSizeAdaptation
from tensorflow_probability.python import distributions  as tfd
from  tensorflow_probability.python import bijectors as tfb
from ..kernels import *
from ..utils import posterior_Gaussian
from .core_model import CoreModel
import warnings


#------------------------------------------------------------------------------
# This script implements a Gaussian process where the posterior distribution of
# the hyperparameters are obtained using MCMC sampling.
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#------------ References ------------------------------------
# - [Rasmussen] "Gaussian process for Machine Learning", C. E. Rasmussen and C. K. I. Williams, MIT press
#-------------------------------------------------------------------------------

#--------- Some notation --------------------#
# loc    := the constant mean function of the Gaussian process
# varm := the variance of the Gaussian process

class BayesianGP():
	def __init__(self, inputs, outputs, kernel_type, noise_level = 1e-3, name = 'model'):
		# Inputs:
		#	inputs := numpy array or tensor array of inputs
		# 	outputs:= numpy array or tensor array of outputs
		# kernel_type := string specifying the type of kernel to be used. Options are
		#                'RBF', 'Matern12', 'Matern32', 'Matern52'
		# noise_level := value for the noise variance of the output
		# name := name for the model
		if '/' in name:
			raise ValueError('Name cannot contain the character /')
		self.name = name

		#------- Storing training data-------------
		self.Xtrain = tf.convert_to_tensor(inputs)
		self.Xtrain = tf.cast(self.Xtrain, tf.float32)
		# checking that the shape of the taining data is at least 2
		self.Xtrain = tf.cond(tf.math.less(tf.size(tf.shape(self.Xtrain)),2),
								lambda: self.Xtrain[:,tf.newaxis],
								lambda: self.Xtrain)
		self.Ytrain = tf.convert_to_tensor(outputs)
		self.Ytrain = tf.cast(self.Ytrain, tf.float32)
		   # the number of training points

		self.dim_input = tf.shape(self.Xtrain)[1].numpy()
		self.n_train = tf.shape(self.Xtrain)[0].numpy() # the number of training points

		self.hypnames_list = [] #list to store the names of the hyperparameters

		# instantiating kernel object
		Kclass = stationary_kernel_mapping[kernel_type]
		self.kernel_name = name + '/kernel'
		self.K = Kclass(self.kernel_name, self.dim_input)
		self.hypnames_list += self.K.get_hypnames()

		# Initializing prior distribution for the constant mean function
		self.rv_loc = tfd.Normal(loc = 0.0, scale = 0.1, name = name + '/rv_loc')
		self.loc_initial_state = 0.0

		self.hypnames_list.append(name+'/loc')

		#-- value for the  variance of the Gaussian noise
		self.noise=  noise_level

		self.jitter_level = 1e-6 # jitter level to deal with numerical instability from cholesky factorization

		return

	def get_batch_and_event_shape(self):
		# function that outputs a dictionary specifying the batch and event shape for
		# the prior distribution of each hyperparameter
		info_dict = {}
		for i in range(2):
			name = self.hypnames_list[i]
			info_dict[name] = {}
			info_dict[name]['batch_shape'] = self.K.priors_list[i].batch_shape
			info_dict[name]['event_shape'] = self.K.priors_list[i].event_shape
		name = self.hypnames_list[-1]
		info_dict[name] = {}
		info_dict[name]['batch_shape'] = self.rv_loc.batch_shape
		info_dict[name]['event_shape'] = self.rv_loc.event_shape
		return info_dict

	def get_initial_states(self):
		# function that outputs a dictionary specifying the initial state for
		# each hyperparameter
		info_dict = {}
		n_hyp = len(self.hypnames_list)
		kernel_initial_state_list = self.K.get_initial_state()
		for i in range(2):
			name = self.hypnames_list[i]
			info_dict[name] = kernel_initial_state_list[i]
		name = self.hypnames_list[-1]
		info_dict[name] = self.loc_initial_state
		return info_dict

	def update_prior(self, hyp_name, new_prior):
		# function used to update the prior distribution of a hyperparameter
		# Inputs
		#  hyp_name := name of the hyperparameter in the model
		#  new_prior := new prior distribution for the hyperparameter
		if hyp_name in self.hypnames_list:
			if hyp_name.endswith('loc'):
				same_batch_shape = (self.rv_loc.batch_shape == new_prior.batch_shape)
				same_event_shape = (self.rv_loc.event_shape == new_prior.event_shape)
				if same_batch_shape and same_event_shape:
					self.rv_loc = new_prior
				else:
					raise ValueError("Incorrect batch shape or event shape.")
			else:
				self.K.update_prior(hyp_name, new_prior)
		else:
			raise NameError('Invalid hyperparameter name.')
		return

	def update_initial_state(self, hyp_name, new_initial_state):
		# function used to update the initial_state of a hyperparameter
		# Inputs
		#  hyp_name := name of the hyperparameter in the model
		#  new_initial_state := new initial_state for the hyperparameter
		if hyp_name in self.hypnames_list:
			if hyp_name.endswith('loc'):
				self.loc_initial_state = new_initial_state
			else:
				self.K.update_initial_state(hyp_name, new_initial_state)
		else:
			raise NameError('Invalid initial state.')
		return



	def joint_log_prob(self, *hyps):
		# function for computing the joint log probability of the Gaussian process
		# model  given values for the noise variance and the hyperparameters
		# Inputs:
		#	noise := value for the Gaussian noise variance
		#   hyps  := list containing values of the hyperparmeters for the kernel
		#            and a value for the constant mean function loc

		#------ forming the kernel -----------------
		Kxx = self.K.compute(self.Xtrain, self.Xtrain, hyps)
		Cov_train = Kxx + (self.noise + self.jitter_level)*tf.eye(self.n_train)

		#-------- Computing the cholesky factor ------------
		L= tf.linalg.cholesky(Cov_train)

		#---- Multivariate normal random variable for the combined outputs -----
		loc = hyps[-1]
		mean = loc*tf.ones(self.n_train, tf.float32)
		rv_observations = tfd.MultivariateNormalTriL(loc = mean, scale_tril = L)

		#--- Collecting the log_probs from the different random variables
		sum_log_prob = (rv_observations.log_prob(self.Ytrain)
					 + self.rv_loc.log_prob(hyps[-1])
					 +  self.K.hyp_loglikelihood(hyps))

		return sum_log_prob


	def mcmc(self, mcmc_samples, num_burnin_steps, num_adaptation_steps, adaptation_rate = 0.40, init_step_size = 0.01, num_leapfrog_steps = 3,thinning = 2):
		# Function used to perform the sampling for the posterior distributions of the hyperparameters
		# Inputs:
		#	mcmc_samples := number of samples to collect for the hyperparameters
		#	num_burnin_steps := number of samples to discard
		#  num_adaptation_steps := number of steps used to execute adaptive HMC
		# 	init_step_size :=  initial step_size for the HMC sampler
		# 	num_leapfrog_steps := number of leapfrog steps for the HMC sampler
		# thinning := number of mcmc samples to skip in between before storing
		# Outputs:
		#	hyp_samples= list of samples for the posterior
		#									distribution of the hyperparameters
		#   acceptance_rate_ := acceptance rate of the sampling

		# unnormalized_posterior_log_prob = functools.partial(self.joint_log_prob, self.noise)
		unnormalized_posterior_log_prob = self.joint_log_prob

		#------- Unconstrained representation---------
		unconstrained_bijectors = []
		unconstrained_bijectors += self.K.get_bijectors()
		unconstrained_bijectors.append(tfb.Identity())

		#---- initial state ---------------------
		initial_state = []
		initial_state += self.K.get_initial_state()
		initial_state.append(self.loc_initial_state)


		# ---- Setting up the mcmc kernel
		kernel_hmc = HamiltonianMonteCarlo(target_log_prob_fn= unnormalized_posterior_log_prob,
										step_size= init_step_size,
										num_leapfrog_steps= num_leapfrog_steps)
		kernel_transformed = TransformedTransitionKernel(inner_kernel= kernel_hmc,
						 bijector = unconstrained_bijectors)
		kernel_adaptive = SimpleStepSizeAdaptation(inner_kernel = kernel_transformed,
										num_adaptation_steps = num_adaptation_steps,
										adaptation_rate=adaptation_rate,
										target_accept_prob=0.65)
		#-- Setting the sample chain -----
		def trace_function(states, previous_kernel_results):
			return previous_kernel_results.inner_results.log_accept_ratio

		@tf.function(autograph=False)
		def  do_sampling():
			return sample_chain(num_results= mcmc_samples,
								num_burnin_steps= num_burnin_steps,
								current_state= initial_state,
								kernel= kernel_transformed,
								trace_fn= trace_function)

		hyp_samples, log_accept_ratio = do_sampling()
		acceptance_ratio = tf.math.exp(tf.minimum(log_accept_ratio, 0.))
		average_acceptance_ratio = tf.math.reduce_mean(acceptance_ratio)

		return hyp_samples, average_acceptance_ratio

	def posteriorGP(self, Xtest, n_test, hyps, fullCov):
		# function computes posterior mean and variance for the  posterior Gaussian process
		# given values for the hyperparameters
		# Inputs:
		# 	 Xtest := N x D tensorflow array of new inputs
		#    n_test := the number of test points
		#	 hyps:= list containing the values for the hyperparameters
		#    fullCov := boolean specifying if a full covariance matrix should be computed or not
		# Output
		#	mean_and_var := array where the first column is an N x 1 array representing the
		#			mean of the posterior Gaussian  distribution and the remaining
		#           columns correspond to:
		#        	   - the (Co)variance which is an N x N array if fullCov = True
		#			   - a N x 1 array of variances if fullCov = False

		# ------- Generate covariance matrix for training data
		Kxx = self.K.compute(self.Xtrain, self.Xtrain, hyps)
		Cov_train = Kxx + (self.noise + self.jitter_level)*tf.eye(self.n_train)
		#----- Computing corresponding Cholesky factor
		L = tf.linalg.cholesky(Cov_train)

		#-------- Generate covariance matrix for test data or array of variance
		if fullCov:
			Kx2 = self.K.compute(Xtest, Xtest, hyps)
			Cov_test = Kx2 +  (self.noise + self.jitter_level)*tf.eye(n_test)
		else:
			Cov_test = self.K.compute_diagonal(Xtest,hyps) + (self.noise + self.jitter_level )*tf.ones(n_test,tf.float32)


		#-------  Generate covariance matrix between training data and test data
		Cov_mixed = self.K.compute(self.Xtrain, Xtest, hyps)

		loc = hyps[-1]
		Y = self.Ytrain[:,tf.newaxis] - loc

		mean_pos, var_pos = posterior_Gaussian(L, Cov_mixed, Cov_test, Y, fullCov)

		mean_pos = mean_pos + loc

		# mean_and_var = tf.concat([mean_pos, var_pos], axis = 1)

		return mean_pos, var_pos

	def predict(self, X, hyp_samples):
		# function computes approximate values for the mean and variance of the posterior
		# distribution of the ouput variable given samples for the hyperparameters
		# of the model
		#Inputs:
		#	X :=  N x D numpy array or tensor array of new inputs
		# 	hyp_samples := list of samples for the hyperparameters of the model
		# Outputs:
		#	mean_pos := the mean for the full posterior Gaussian process (vector of length N)
		#  var_pos:= the variance for the full posterior Gaussian process (vector of length N)


		Xtest = tf.convert_to_tensor(X)
		Xtest = tf.cast(Xtest, tf.float32)
		# checking that the shape of the input is at least 2
		Xtest= tf.cond(tf.math.less(tf.size(tf.shape(Xtest)),2),
								lambda: Xtest[:,tf.newaxis],
								lambda: Xtest)

		n_hyp = len(hyp_samples)
		n_s = tf.shape(hyp_samples[0])[0].numpy()
		n_test = tf.shape(Xtest)[0].numpy()

		# setting up the tf.while_loop
		i0 = tf.constant(0)
		mean0 = tf.zeros([1,n_test,1])
		var0 = tf.ones([1, n_test,1])
		condition_function = lambda i, mean, var: i < n_s


		def body_function(i, mean, var):
			hyps = [tf.gather(hyp_samples[j],i, axis=0) for j in range(n_hyp)]
			new_mean, new_var =self.posteriorGP(Xtest, n_test, hyps, False)
			new_mean = new_mean[tf.newaxis,:,:]
			new_var = new_var[tf.newaxis,:,:]
			return [i+1, tf.concat([mean, new_mean], axis=0), tf.concat([var, new_var], axis=0)]


		@tf.function(autograph=False)
		def get_results():
			return tf.while_loop(condition_function,
										body_function,
										loop_vars = [i0, mean0, var0],
										shape_invariants = [i0.get_shape(),
															tf.TensorShape([None,n_test,1]),
															tf.TensorShape([None,n_test,1])],
										parallel_iterations= n_s)


		_, mean_samples, var_samples = get_results()

		mean_samples = mean_samples[1:,:,:]
		var_samples = var_samples[1:,:,:]

		mean_pos = tf.math.reduce_mean(mean_samples, axis =0)
		var_pos = tf.math.reduce_mean(var_samples, axis =0) + tf.math.reduce_variance(mean_samples, axis = 0)

		return mean_pos, var_pos

	# #---------------------------------------------------------------------------------------------
	# #---- The next functions are needed for sensitivity analysis of the latent output (i.e no noise is added to the output)

	def full_posteriorGP(self, X, L, hyps):
		# This is needed for computing the main effecs and interactions
		# Inputs:
		#	X:= is a 2-dimensional array
		#	L:= Cholesky factor of the Covariance matrix of the training data
		#   hyps  := list containing values of the hyperparmeters for the kernel
		#            and a value for the constant mean function loc

		Cov_test = self.K.compute_diagonal(X, hyps)

		Cov_mixed = self.K.compute(self.Xtrain, X, hyps)

		loc = hyps[-1]
		Y = self.Ytrain[:, tf.newaxis] - loc

		mean_pos, var_pos = posterior_Gaussian(L, Cov_mixed, Cov_test, Y, False)

		mean_pos = mean_pos + loc

		return mean_pos, var_pos


	def marginal_posteriorGP(self, X, n_s, L, hyps, Cov_expected):
		# This computes the marginal posterior Gaussian E[Y|Xp = xp] or
		# Inputs:
		#	X:= is an array of input samples
		#   n_s = an integer that is less than or equal to len(X)
		#	L:= Cholesky factor of the Covariance matrix of the training data
		#   hyps  := list containing values of the hyperparmeters for the kernel
		#            and a value for the constant mean function loc
		# Cov_expected := expected covariance value for the stationary kernel
		# Outputs:
		#      mean_pos := mean of the Gaussian E[Y|Xp = xp]
		#      var_pos  := variance of the Gaussian E[Y|Xp = xp]
		i0 = tf.constant(0)
		mean0 = tf.zeros([1,1])
		var0 = tf.ones([1,1])

		condition_function = lambda i, mean, var: tf.math.less(i, n_s)
		loc = hyps[-1]
		def body_function(i, mean, var):
			Xused  = tf.gather(X,i, axis = 0)
			Cov_mixed = self.K.compute(self.Xtrain, Xused, hyps)
			Cov_mixed_expected = tf.reduce_mean(Cov_mixed, axis = -1, keepdims = True)
			Y = self.Ytrain[:, tf.newaxis] - loc
			new_mean, new_var = posterior_Gaussian(L, Cov_mixed_expected, Cov_expected, Y, False)
			new_var = tf.maximum(new_var, 1e-12)
			new_mean = new_mean + loc
			return [i+1, tf.concat([mean, new_mean], axis=0), tf.concat([var, new_var], axis=0)]


		_, mean, var = tf.while_loop(condition_function,
						body_function,
						loop_vars = [i0, mean0, var0],
						shape_invariants = [i0.get_shape(),
											tf.TensorShape([None,1]),
											tf.TensorShape([None,1])],
						parallel_iterations = n_s)

		mean = mean[1:,:]
		var = var[1:,:]

		return mean, var


	def compute_marginal_posteriorGP(self, sampling_dict, hyps, reduced = True):
		# function used to compute the mean and variance of marginal Gaussian processes
		#  of the form
		#          E[Y|X_p]
		# This means that we are keeping a set of variables fixed (in this case the
		# subset X_p) while averaging out over the rest of the variables. For simplicity,
		# the variables are assumed to have uniform distributions. The integrals involved
		# in the computation are approximated with Monte Carlo integration
		# Inputs:
		#   sampling_dict := dictionary containing  input samples and information about the samples
		#   hyps := list of tensor values for the hyperparameters for the model

		# ------- generate covariance matrix for training data and computing the corresponding cholesky factor
		Kxx = self.K.compute(self.Xtrain, self.Xtrain, hyps)
		Cov_train = Kxx + (self.noise + self.jitter_level)*tf.eye(self.n_train)
		L = tf.linalg.cholesky(Cov_train)
		outputs = {}

		@tf.function(autograph=False)
		def main_function(Xin, n_in, Cov_expected_in):
			return self.marginal_posteriorGP(Xin, n_in, L, hyps, Cov_expected_in)

		for key in sampling_dict.keys():
			n_s = len(sampling_dict[key]['X_sampling'])
			X = tf.convert_to_tensor(sampling_dict[key]['X_sampling'], tf.float32)
			diff_samples = tf.convert_to_tensor(sampling_dict[key]['diff_samples'], tf.float32)
			marg_dims = tf.convert_to_tensor(sampling_dict[key]['marg_indices_list'], tf.int32)
			Cov_expected, _ = self.K.compute_marginal(diff_samples, hyps, marg_dims)
			mean, var = main_function(X, n_s, Cov_expected)
			if reduced:
				outputs[key] = tf.reduce_mean(tf.math.square(mean) + var).numpy()
			else:
				outputs[key] = {}
				outputs[key]['grid_points'] = sampling_dict[key]['X_grid']
				outputs[key]['mean'] = mean.numpy()
				outputs[key]['var'] = var.numpy()
		return outputs



	def compute_full_posteriorGP(self, Xsamples, hyps, reduced = True):
		# function used to compute the mean and variance of the full posterior Gaussian process
		# Inputs:
		#  Xsamples := 2-dimensionalarray containing the input samples
		#  hyps := list of hyperparameters for the model

		# ------- generate covariance matrix for training data and computing the corresponding cholesky factor
		Kxx = self.K.compute(self.Xtrain, self.Xtrain, hyps)
		Cov_train = Kxx + (self.noise + self.jitter_level)*tf.eye(self.n_train)
		L = tf.linalg.cholesky(Cov_train)

		@tf.function(autograph=False)
		def main_function(Xin):
			return self.full_posteriorGP(Xin, L, hyps)

		X = tf.convert_to_tensor(Xsamples, tf.float32)
		mean, var = main_function(X)

		if reduced:
			return tf.reduce_mean(tf.math.square(mean) + var).numpy()
		else:
			return mean.numpy(), var.numpy()
