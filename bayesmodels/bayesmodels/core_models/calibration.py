import tensorflow as tf
import numpy as np
import functools
import math
from tensorflow_probability.python.mcmc import sample_chain, HamiltonianMonteCarlo
from tensorflow_probability.python.mcmc import  TransformedTransitionKernel, SimpleStepSizeAdaptation
from tensorflow_probability.python import distributions  as tfd
from  tensorflow_probability.python import bijectors as tfb
from ..kernels import *
from ..utils import posterior_Gaussian
from .core_model import CoreModel
import warnings


#------------------------------------------------------------------------------
# This script implements a version of the Kennedy O' Hagan model. This consists
# of modeling the output of a physical process in the form
#  y  = eta(x, theta) + delta(x)
# where eta(.,.) is a Gaussian process that models the output of a simulator of the
# physical process and delta() is another Gaussian process that models the
# discrepancy between the simulator Gaussian process and the actual process.
# The variables theta represent the "best values" for the calibration parameters.
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------


#------------ References ------------------------------------
# - [Rasmussen] "Gaussian process for Machine Learning", C. E. Rasmussen and C. K. I. Williams, MIT press
# - [Higdon]   "Combining Field Data and Computer Simulation for Calibration and Prediction", D. Higdon,
# M. Kennedy, J. Cavendish, J. Cafeo and R.D. Ryne

#-------------------------------------------------------------------------------

#--------- Some notation --------------------#
# loc    := the constant mean function of the simulator Gaussian process
# varsim := the variance for the simulator Gaussian process
# vard   := the variance for the discrepancy Gaussian process


class Calibration():
	def __init__(self, sim_inputs, sim_outputs, exp_inputs, exp_outputs, lower_bounds, upper_bounds, kernel_types = ['RBF', 'RBF', 'RBF'], noise_level = 1e-3, name = 'model'):
		# Inputs:
		# sim_inputs , sim_outputs := inputs and outputs for the simulation data. These are numpy arrays or tensor arrays
		# 						Here the sim_inputs array includes the regular input variables
		# 						as well as the parameter variables
		# exp_inputs , exp_outputs := inputs and outputs for the experiment
		# lower_bounds and upper_bounds := lists containing the bounds for the uniform distribution for
		# 				the calibration parameters. For now, we will assume that the prior distributions for
		# 				the calibration parameters are uniform distributions
		#kernel_types:= list of strings specifying the types of kernel to be used. Options are
		#                'RBF', 'Matern12', 'Matern32', 'Matern52'
		# noise_level := value for the noise variance of the output

		if '/' in name:
			raise ValueError('Name cannot contain the character /')
		self.name = name

		# -------- Storing the training data ----------------------------------
		#1) experimental  data
		self.Xexp = tf.convert_to_tensor(exp_inputs)
		self.Xexp = tf.cast(self.Xexp, tf.float32)
		# checking that the shape of the experimental input training data is at least 2
		self.Xexp = tf.cond(tf.math.less(tf.size(tf.shape(self.Xexp)),2),
								lambda: self.Xexp[:,tf.newaxis],
								lambda: self.Xexp)
		self.Yexp  = tf.convert_to_tensor(exp_outputs)
		self.Yexp  = tf.cast(self.Yexp, tf.float32)

		self.dim_features = tf.shape(self.Xexp)[1].numpy()
		#2) simulation data
		Sim = tf.convert_to_tensor(sim_inputs)
		Sim = tf.cast(Sim, tf.float32)
		self.dim_par = tf.shape(Sim)[1].numpy() - self.dim_features
		self.Xsim = Sim[:,:self.dim_features]
		self.Psim = Sim[:,self.dim_features:]
		self.Ysim  = tf.convert_to_tensor(sim_outputs)
		self.Ysim  = tf.cast(self.Ysim, tf.float32)

		self.n_sim = tf.shape(Sim)[0].numpy() # the number of simulation  training points
		self.n_exp = tf.shape(self.Xexp)[0].numpy() # the number of experimental  training points

		self.Xaug = tf.concat([self.Xsim, self.Xexp], axis = 0)
		self.Yaug = tf.concat([self.Ysim, self.Yexp], axis = 0)

		self.n_total = self.n_sim + self.n_exp

		self.hypnames_list = [] #list to store the names of the hyperparameters

		# instantiating kernel objects
		# 1) kernel for features of simulation GP
		K_class = stationary_kernel_mapping[kernel_types[0]]
		self.simft_kernel_name = name + '/sim_kernel/features_hyp'
		self.K_simft = K_class(self.simft_kernel_name, self.dim_features)
		self.hypnames_list += self.K_simft.get_hypnames()
		# 2) kernel for parameters of simulation GP
		K_class = stationary_kernel_mapping[kernel_types[1]]
		self.simpar_kernel_name = name + '/sim_kernel/parameters_hyp'
		self.K_simpar = K_class(self.simpar_kernel_name, self.dim_par)
		self.hypnames_list += self.K_simpar.get_hypnames()
		# Ignore variance of K_simpar
		self.hypnames_list.pop()
		# 3) kernel for discrepancy GP
		K_class = stationary_kernel_mapping[kernel_types[2]]
		self.disc_kernel_name = name + '/disc_kernel'
		self.K_disc = K_class(self.disc_kernel_name, self.dim_features)
		self.hypnames_list += self.K_disc.get_hypnames()

		# Initializing prior distribution  and initialization for the calibration parameters
		self.rv_par = tfd.Independent(tfd.Uniform(low = lower_bounds, high = upper_bounds),
                        reinterpreted_batch_ndims=1, name= name + '/rv_par')
		self.par_initial_state = (np.array(lower_bounds) + np.array(upper_bounds))/2.0
		self.par_initial_state = tf.convert_to_tensor(self.par_initial_state)
		self.par_initial_state = tf.cast(self.par_initial_state, tf.float32)

		self.hypnames_list.append(name + '/par')

		# Initializing prior distribution  and initialization for the constant mean function
		self.rv_loc = tfd.Normal(loc = 0.0, scale = 0.1, name = name + '/rv_loc')
		self.loc_initial_state = 0.0
		self.hypnames_list.append(name+'/loc')

		#-- value for the  variance of the Gaussian noise
		self.noise=  noise_level

		self.jitter_level = 1e-6 # jitter level to deal with numerical instability from cholesky factorization

		return


	def get_batch_and_event_shape(self):
		# function that outputs a dictionary specifying the batch and event shape for
		# the prior distribution of each hyperparameter
		info_dict = {}
		# dealing with the first simulation kernel
		for i in range(2):
			name = self.hypnames_list[i]
			if name.endswith("vark"):
				name =  self.name + '/sim_kernel/vark'
			info_dict[name] = {}
			info_dict[name]['batch_shape'] = self.K_simft.priors_list[i].batch_shape
			info_dict[name]['event_shape'] = self.K_simft.priors_list[i].event_shape
		# dealing with the second simulation kernel
		name = self.hypnames_list[2]
		info_dict[name] = {}
		info_dict[name]['batch_shape'] = self.K_simpar.priors_list[0].batch_shape
		info_dict[name]['event_shape'] = self.K_simpar.priors_list[0].event_shape
		# dealing with the discrepancy kernel
		for i in range(2):
			name = self.hypnames_list[3+i]
			info_dict[name] = {}
			info_dict[name]['batch_shape'] = self.K_disc.priors_list[i].batch_shape
			info_dict[name]['event_shape'] = self.K_disc.priors_list[i].event_shape
		# dealing with the calibration parameters
		name = self.hypnames_list[-2]
		info_dict[name] = {}
		info_dict[name]['batch_shape'] = self.rv_par.batch_shape
		info_dict[name]['event_shape'] = self.rv_par.event_shape
		# dealing with loc
		name = self.hypnames_list[-1]
		info_dict[name] = {}
		info_dict[name]['batch_shape'] = self.rv_loc.batch_shape
		info_dict[name]['event_shape'] = self.rv_loc.event_shape
		return info_dict

	def describe_priors(self):
		# function that outputs a dictionary specifying the batch and event shape for
		# the prior distribution of each hyperparameter
		info_dict = {}
		# dealing with the first simulation kernel
		for i in range(2):
			name = self.hypnames_list[i]
			if name.endswith("vark"):
				name =  self.name + '/sim_kernel/vark'
			info_dict[name] = str(self.K_simft.priors_list[i])
		# dealing with the second simulation kernel
		name = self.hypnames_list[2]
		info_dict[name] = str(self.K_simpar.priors_list[0])
		# dealing with the discrepancy kernel
		for i in range(2):
			name = self.hypnames_list[3+i]
			info_dict[name] = str(self.K_disc.priors_list[i])
		# dealing with the calibration parameters
		name = self.hypnames_list[-2]
		info_dict[name] = str(self.rv_par)
		# dealing with loc
		name = self.hypnames_list[-1]
		info_dict[name] = str(self.rv_loc)
		return info_dict

	def compute_moments_of_priors(self):
		# function that outputs a dictionary specifying the batch and event shape for
		# the prior distribution of each hyperparameter
		info_dict = {}
		# dealing with the first simulation kernel
		for i in range(2):
			name = self.hypnames_list[i]
			if name.endswith("vark"):
				name =  self.name + '/sim_kernel/vark'
			info_dict[name] = {}
			info_dict[name]['mean'] = self.K_simft.priors_list[i].mean().numpy()
			info_dict[name]['variance'] = self.K_simft.priors_list[i].variance().numpy()
		# dealing with the second simulation kernel
		name = self.hypnames_list[2]
		info_dict[name] = {}
		info_dict[name]['mean'] = self.K_simpar.priors_list[0].mean().numpy()
		info_dict[name]['variance'] = self.K_simpar.priors_list[0].variance().numpy()
		# dealing with the discrepancy kernel
		# dealing with the discrepancy kernel
		for i in range(2):
			name = self.hypnames_list[3+i]
			info_dict[name] = {}
			info_dict[name]["mean"] = self.K_disc.priors_list[i].mean().numpy()
			info_dict[name]["variance"] = self.K_disc.priors_list[i].variance().numpy()
		# dealing with the calibration parameters
		name = self.hypnames_list[-2]
		info_dict[name] = {}
		info_dict[name]["mean"] = self.rv_par.mean().numpy()
		info_dict[name]["variance"] = self.rv_par.variance().numpy()
		# dealing with loc
		name = self.hypnames_list[-1]
		info_dict[name] = {}
		info_dict[name]["mean"] = self.rv_loc.mean().numpy()
		info_dict[name]["variance"] = self.rv_loc.variance().numpy()
		return info_dict

	def get_initial_state(self):
		# function that outputs a dictionary specifying the initial state for
		# each hyperparameter
		info_dict = {}
		# dealing with the first simulation kernel
		kernel_initial_state_list = self.K_simft.get_initial_state()
		for i in range(2):
			name = self.hypnames_list[i]
			info_dict[name] = kernel_initial_state_list[i]
		if name.endswith("vark"):
			info_dict[self.name + '/sim_kernel/vark'] = info_dict.pop(name)
		# dealing with the second simulation kernel
		kernel_initial_state_list = self.K_simpar.get_initial_state()
		name = self.hypnames_list[2]
		info_dict[name] = kernel_initial_state_list[0]
		kernel_initial_state_list = self.K_disc.get_initial_state()
		for i in range(2):
			name = self.hypnames_list[3+i]
			info_dict[name] = kernel_initial_state_list[i]
		# dealing with calibration parameters
		name = self.hypnames_list[-2]
		info_dict[name] = self.par_initial_state
		# dealing with loc
		name = self.hypnames_list[-1]
		info_dict[name] = self.loc_initial_state

		return info_dict

	def update_prior(self, hyp_name, new_prior):
		# function used to update the prior distribution of a hyperparameter
		# Inputs
		#  hyp_name := name of the hyperparameter in the model
		#  new_prior := new prior distribution for the hyperparameter
		if hyp_name in self.hypnames_list:
			if hyp_name.endswith('loc'):
				same_batch_shape = (self.rv_loc.batch_shape == new_prior.batch_shape)
				same_event_shape = (self.rv_loc.event_shape == new_prior.event_shape)
				if same_batch_shape and same_event_shape:
					self.rv_loc = new_prior
				else:
					raise ValueError("Incorrect batch shape or event shape.")
			elif hyp_name.endswith('par'):
				same_batch_shape = (self.rv_par.batch_shape == new_prior.batch_shape)
				same_event_shape = (self.rv_par.event_shape == new_prior.event_shape)
				if same_batch_shape and same_event_shape:
					self.rv_par = new_prior
				else:
					raise ValueError("Incorrect batch shape or event shape.")
			elif hyp_name.startswith(self.name + '/sim_kernel/features_hyp'):
				self.K_simft.update_prior(hyp_name, new_prior)
			elif hyp_name.startswith(self.name + '/sim_kernel/parameters_hyp'):
				self.K_simpar.update_prior(hyp_name, new_prior)
			else:
				# must be discrepancy kernel
				self.K_disc.update_prior(hyp_name, new_prior)
		elif hyp_name == self.name + '/sim_kernel/vark':
			self.K_simft.update_prior(hyp_name, new_prior)
		else:
			raise NameError('Invalid hyperparameter name.')
		return

	def update_initial_state(self, hyp_name, new_initial_state):
		if hyp_name in self.hypnames_list:
			if hyp_name.endswith('loc'):
				self.loc_initial_state = new_initial_state
			elif hyp_name.endswith('par'):
				self.par_initial_state = new_initial_state
			elif hyp_name.startswith(self.name + '/sim_kernel/features_hyp'):
				self.K_simft.update_initial_state(hyp_name, new_initial_state)
			elif hyp_name.startswith(self.name + '/sim_kernel/parameters_hyp'):
				self.K_simpar.update_initial_state(hyp_name, new_initial_state)
			else:
				# must be discrepancy kernel
				self.K_disc.update_initial_state(hyp_name, new_initial_state)
		elif hyp_name == self.name + '/sim_kernel/vark':
			self.K_simft.update_initial_state(self.name + '/sim_kernel/features_hyp/vark', new_initial_state)
		else:
			raise NameError('Invalid initial state.')
		return


	def joint_log_prob(self, *hypars):
		# function for computing the joint log probability of the Gaussian process
		# model  given values for the noise variance and the hyperparameters
		# Inputs:
		#	noise := value for the Gaussian noise variance
		#   hypars  := list containing values of the hyperparmeters for the kernel, values
		#             for the calibration parameters
		#            and a value for the constant mean function loc


		# #------ forming the kernels -----------------
		Kxx = self.K_simft.compute(self.Xaug, self.Xaug, hypars, indices =[0,1])

		par = hypars[-2]
		par_tiled = tf.tile(par[tf.newaxis,:], [self.n_exp,1])
		Paug = tf.concat([self.Psim, par_tiled], axis = 0)
		Kpp = self.K_simpar.compute(Paug, Paug, [hypars[2], 1.0], indices = [0, 1])
		Cov_sim = tf.math.multiply(Kxx, Kpp)

		Kxx_disc = self.K_disc.compute(self.Xexp, self.Xexp, hypars, indices = [3,4])

		Cov_err = tf.pad(Kxx_disc, tf.constant([[self.n_sim,0],[self.n_sim,0]]), name = None, constant_values=0)

		#-------- Computing the cholesky factor ------------
		Cov = Cov_sim + Cov_err + (self.noise + self.jitter_level)*tf.eye(self.n_total)
		L = tf.linalg.cholesky(Cov)

		#---- Multivariate normal random variable for the combined outputs -----
		loc = hypars[-1]
		mean = loc*tf.ones(self.n_total, tf.float32)
		rv_observations = tfd.MultivariateNormalTriL(loc = mean, scale_tril = L )

		#--- Collecting the log_probs from the different random variables
		sum_log_prob = (rv_observations.log_prob(self.Yaug) + self.rv_loc.log_prob(loc)
						+ self.rv_par.log_prob(par) + self.K_simft.hyp_loglikelihood(hypars, indices = [0,1])
						+ self.K_disc.hyp_loglikelihood(hypars, indices = [3,4]) + self.K_simpar.priors_list[0].log_prob(hypars[2]))

		return sum_log_prob


	def mcmc(self, mcmc_samples, num_burnin_steps, num_adaptation_steps, adaptation_rate = 0.40, init_step_size = 0.01, num_leapfrog_steps = 3,thinning = 2):
		# Function used to perform the sampling for the posterior distributions of the hyperparameters
		# Inputs:
		#	mcmc_samples := number of samples to collect for the hyperparameters
		#	num_burnin_steps := number of samples to discard
		#  num_adaptation_steps := number of steps used to execute adaptive HMC
		# 	init_step_size :=  initial step_size for the HMC sampler
		# 	num_leapfrog_steps := number of leapfrog steps for the HMC sampler
		# thinning := number of mcmc samples to skip in between before storing
		# Outputs:
		#	hyp_samples= list of samples for the posterior
		#									distribution of the hyperparameters
		#   acceptance_rate_ := acceptance rate of the sampling

		# unnormalized_posterior_log_prob = functools.partial(self.joint_log_prob, self.noise)
		unnormalized_posterior_log_prob = self.joint_log_prob

		#------- Unconstrained representation---------
		unconstrained_bijectors = []
		#  first simulation kernel
		unconstrained_bijectors += self.K_simft.get_bijectors()
		#  second simulation kernel
		unconstrained_bijectors += self.K_simpar.get_bijectors()
		unconstrained_bijectors.pop()
		#  discrepancy kernel
		unconstrained_bijectors += self.K_simpar.get_bijectors()

		unconstrained_bijectors += [tfb.Identity(), tfb.Identity()]

		#---- initial state ---------------------
		initial_state = []
		#  first simulation kernel
		initial_state += self.K_simft.get_initial_state()
		#  second simulation kernel
		initial_state += self.K_simpar.get_initial_state()
		initial_state.pop()
		#  discrepancy kernel
		initial_state += self.K_disc.get_initial_state()

		initial_state += [self.par_initial_state, self.loc_initial_state]


		# ---- Setting up the mcmc kernel
		kernel_hmc = HamiltonianMonteCarlo(target_log_prob_fn= unnormalized_posterior_log_prob,
										step_size= init_step_size,
										num_leapfrog_steps= num_leapfrog_steps)
		kernel_transformed = TransformedTransitionKernel(inner_kernel= kernel_hmc,
						 bijector = unconstrained_bijectors)
		kernel_adaptive = SimpleStepSizeAdaptation(inner_kernel = kernel_transformed,
										num_adaptation_steps = num_adaptation_steps,
										adaptation_rate=adaptation_rate,
										target_accept_prob=0.65)
		#-- Setting the sample chain -----
		def trace_function(states, previous_kernel_results):
			return previous_kernel_results.inner_results.log_accept_ratio

		@tf.function(autograph=False)
		def  do_sampling():
			return sample_chain(num_results= mcmc_samples,
								num_burnin_steps= num_burnin_steps,
								current_state= initial_state,
								kernel= kernel_transformed,
								trace_fn= trace_function)

		hypar_samples, log_accept_ratio = do_sampling()
		acceptance_ratio = tf.math.exp(tf.minimum(log_accept_ratio, 0.))
		average_acceptance_ratio = tf.math.reduce_mean(acceptance_ratio)

		return hypar_samples, average_acceptance_ratio



	def fullposteriorGP(self, Xtest,  n_test,  hypars, fullCov = False):
		# generate posterior samples for the full process, given values for the parameters and hyperparameters
		# Inputs:
		# 	Xtest := N x D tensorflow array of new inputs
		#  n_test := the number of test point
		#	hypars:= list of hyperparmaters and calibration parameters
		# fullCov := boolean specifying if a full covariance matrix should be computed or not
		# Output
		#	mean_and_var := array where the first column is an N x 1 array representing the
		#					mean of the posterior Gaussian  distribution and the remaining columns correspond to
		#        			the (Co)variance which is an N x N array if
		#					fullCov = True or a N x 1 array if fullCov = False


		# ------- generate covariance matrix for training data and computing the corresponding cholesky factor
		Kxx = self.K_simft.compute(self.Xaug, self.Xaug, hypars, indices =[0,1])

		par = hypars[-2]
		par_tiled = tf.tile(par[tf.newaxis,:], [self.n_exp,1])
		Paug = tf.concat([self.Psim, par_tiled], axis = 0)
		Kpp = self.K_simpar.compute(Paug, Paug, [hypars[2], 1.0], indices = [0, 1])
		Cov_sim = tf.math.multiply(Kxx, Kpp)

		Kxx_disc = self.K_disc.compute(self.Xexp, self.Xexp, hypars, indices = [3,4])

		Cov_disc = tf.pad(Kxx_disc, tf.constant([[self.n_sim,0],[self.n_sim,0]]), name = None, constant_values=0)

		#-------- Computing the cholesky factor ------------
		Cov_train = Cov_sim + Cov_disc + (self.noise + self.jitter_level)*tf.eye(self.n_total)
		L = tf.linalg.cholesky(Cov_train)

		#-------- generate covariance matrix for test data
		Paug2 = tf.tile(par[tf.newaxis,:], [n_test,1])
		if fullCov:
			Cov_sim2 = self.K_simft.compute(Xtest, Xtest, hypars, indices = [0,1])
			Cov_disc2 =  self.K_disc.compute(Xtest, Xtest, hypars, indices = [3,4])
			Cov_test = Cov_sim2 + Cov_disc2 + (self.noise + self.jitter_level)*tf.eye(n_test)
		else:
			Cov_test =  self.K_simft.compute_diagonal(Xtest,hypars, indices =[0,1]) + self.K_disc.compute_diagonal(Xtest,hypars, indices =[3,4]) + (self.noise + self.jitter_level )*tf.ones(n_test)

		#------- covariance between test data and training data
		Kx3 =  self.K_simft.compute(self.Xaug, Xtest, hypars, indices =[0,1])
		Kp3 = self.K_simpar.compute(Paug, Paug2, [hypars[2], 1.0], indices = [0, 1])
		Cov_sim3 = tf.multiply(Kx3, Kp3)

		Cov_disc3 = self.K_disc.compute(self.Xexp, Xtest, hypars, indices = [3,4])
		Cov_disc3 = tf.pad(Cov_disc3, tf.constant([[self.n_sim,0],[0,0]]), name = None, constant_values=0)

		Cov_mixed = Cov_sim3 + Cov_disc3
		loc = hypars[-1]
		Y = self.Yaug[:, tf.newaxis] - loc


		mean_pos, var_pos = posterior_Gaussian(L, Cov_mixed, Cov_test, Y, fullCov)

		mean_pos = mean_pos + loc

		return mean_pos, var_pos

	def simposteriorGP(self, Xtest, n_test,  hypars, fullCov = False):
		# generate posterior samples for the simulation process, given values for the parameters and the hyperparameters
		# Inputs:
		# 	Xtest := N x D tensorflow array of new inputs
		#  n_test := the number of test point
		#	hypars:= list of hyperparmaters and calibration parameters
		# fullCov := boolean specifying if a full covariance matrix should be computed or not
		# Output
		#	mean_and_var := array where the first column is an N x 1 array representing the
		#					mean of the posterior Gaussian  distribution and the remaining columns correspond to
		#        			the (Co)variance which is an N x N array if
		#					fullCov = True or a N x 1 array if fullCov = False

		# ------- generate covariance matrix for simulation training data and computing the corresponding cholesky factor
		Kxx = self.K_simft.compute(self.Xaug, self.Xaug, hypars, indices =[0,1])

		par = hypars[-2]
		par_tiled = tf.tile(par[tf.newaxis,:], [self.n_exp,1])
		Paug = tf.concat([self.Psim, par_tiled], axis = 0)
		Kpp = self.K_simpar.compute(Paug, Paug, [hypars[2], 1.0], indices = [0, 1])
		Cov_sim = tf.math.multiply(Kxx, Kpp)

		#-------- Computing the cholesky factor ------------
		Cov_train = Cov_sim + (self.noise + self.jitter_level)*tf.eye(self.n_total)
		L = tf.linalg.cholesky(Cov_train)

		#-------- generate covariance matrix for test data
		Paug2 = tf.tile(par[tf.newaxis,:], [n_test,1])
		if fullCov:
			Cov_sim2 = self.K_simft.compute(Xtest, Xtest, hypars, indices = [0,1])
			Cov_test = Cov_sim2 + (self.noise + self.jitter_level)*tf.eye(n_test)
		else:
			Cov_test =  self.K_simft.compute_diagonal(Xtest,hypars, indices =[0,1])  + (self.noise + self.jitter_level )*tf.ones(n_test)

		#------- covariance between test data and training data
		Kx3 =  self.K_simft.compute(self.Xaug, Xtest, hypars, indices =[0,1])
		Kp3 = self.K_simpar.compute(Paug, Paug2, [hypars[2], 1.0], indices = [0, 1])
		Cov_sim3 = tf.multiply(Kx3, Kp3)
		Cov_mixed = Cov_sim3

		loc = hypars[-1]
		Y = self.Yaug[:, tf.newaxis] - loc


		mean_pos, var_pos = posterior_Gaussian(L, Cov_mixed, Cov_test, Y, fullCov)

		mean_pos = mean_pos + loc

		return mean_pos, var_pos



	def discrepancyposteriorGP(self, Xtest,  n_test,  hypars, fullCov = False):
		# generate posterior samples for the discrepancy Gaussian process, given values for the parameters and the hyperparameters
		# Inputs:
		# 	Xtest := N x D tensorflow array of new inputs
		#  n_test := the number of test point
		#	hypars:= list of hyperparmaters and calibration parameters
		# fullCov := boolean specifying if a full covariance matrix should be computed or not
		# Output
		#	mean_and_var := array where the first column is an N x 1 array representing the
		#					mean of the posterior Gaussian  distribution and the remaining columns correspond to
		#        			the (Co)variance which is an N x N array if
		#					fullCov = True or a N x 1 array if fullCov = False


		# ------- generate covariance matrix for training data and computing the corresponding cholesky factor
		Kxx = self.K_simft.compute(self.Xaug, self.Xaug, hypars, indices =[0,1])

		par = hypars[-2]
		par_tiled = tf.tile(par[tf.newaxis,:], [self.n_exp,1])
		Paug = tf.concat([self.Psim, par_tiled], axis = 0)
		Kpp = self.K_simpar.compute(Paug, Paug, [hypars[2], 1.0], indices = [0, 1])
		Cov_sim = tf.math.multiply(Kxx, Kpp)

		Kxx_disc = self.K_disc.compute(self.Xexp, self.Xexp, hypars, indices = [3,4])

		Cov_disc = tf.pad(Kxx_disc, tf.constant([[self.n_sim,0],[self.n_sim,0]]), name = None, constant_values=0)

		#-------- Computing the cholesky factor ------------
		Cov_train = Cov_sim + Cov_disc + (self.noise + self.jitter_level)*tf.eye(self.n_total)
		L = tf.linalg.cholesky(Cov_train)

		#-------- generate covariance matrix for test data
		Paug2 = tf.tile(par[tf.newaxis,:], [n_test,1])
		if fullCov:
			Cov_disc2 =  self.K_disc.compute(Xtest, Xtest, hypars, indices = [3,4])
			Cov_test = Cov_disc2 + (self.jitter_level)*tf.eye(n_test)
		else:
			Cov_test = self.K_disc.compute_diagonal(Xtest,hypars, indices =[3,4]) + (self.jitter_level )*tf.ones(n_test)

		#------- covariance between test data and experimental training data
		Cov_disc3 = self.K_disc.compute(self.Xexp, Xtest, hypars, indices = [3,4])
		Cov_disc3 = tf.pad(Cov_disc3, tf.constant([[self.n_sim,0],[0,0]]), name = None, constant_values=0)
		Cov_mixed = Cov_disc3

		loc = hypars[-1]
		Y = self.Yaug[:, tf.newaxis] - loc

		mean_pos, var_pos = posterior_Gaussian(L, Cov_mixed, Cov_test, Y, fullCov)


		return mean_pos, var_pos



	def predict(self, X, hypar_samples, type = "full"):
		# function computes approximate values for the mean and variance of the posterior
		# distribution of the ouput variable given samples for the hyperparameters
		# of the model
		#Inputs:
		#	X :=  N x D numpy array or tensor array of new inputs
		# 	hyp_samples := list of samples for the hyperparameters of the model
		# Outputs:
		#	mean_pos := the mean for the full posterior Gaussian process (vector of length N)
		#  var_pos := the variance for the full posterior Gaussian process (vector of length N)

		if type == "full":
			generating_function = self.fullposteriorGP
		elif type == "simulator":
			generating_function = self.simposteriorGP
		elif type == "discrepancy":
			generating_function = self.discrepancyposteriorGP
		else:
			raise Exception("Invalid type")

		Xtest = tf.convert_to_tensor(X)
		Xtest = tf.cast(Xtest, tf.float32)
		# checking that the shape of the input is at least 2
		Xtest= tf.cond(tf.math.less(tf.size(tf.shape(Xtest)),2),
								lambda: Xtest[:,tf.newaxis],
								lambda: Xtest)

		n_hyp = len(hypar_samples)
		n_s = tf.shape(hypar_samples[0])[0].numpy()
		n_test = tf.shape(Xtest)[0].numpy()

		# setting up the tf.while_loop
		i0 = tf.constant(0)
		mean0 = tf.zeros([1,n_test,1])
		var0 = tf.ones([1, n_test,1])
		condition_function = lambda i, mean, var: i < n_s


		def body_function(i, mean, var):
			hypars = [tf.gather(hypar_samples[j],i, axis=0) for j in range(n_hyp)]
			new_mean, new_var = generating_function(Xtest, n_test, hypars, False)
			new_mean = new_mean[tf.newaxis,:,:]
			new_var = new_var[tf.newaxis,:,:]
			return [i+1, tf.concat([mean, new_mean], axis=0), tf.concat([var, new_var], axis=0)]


		@tf.function(autograph=False)
		def get_results():
			return tf.while_loop(condition_function,
										body_function,
										loop_vars = [i0, mean0, var0],
										shape_invariants = [i0.get_shape(),
															tf.TensorShape([None,n_test,1]),
															tf.TensorShape([None,n_test,1])],
										parallel_iterations= n_s)


		_, mean_samples, var_samples = get_results()

		mean_samples = mean_samples[1:,:,:]
		var_samples = var_samples[1:,:,:]

		mean_pos = tf.math.reduce_mean(mean_samples, axis =0)
		var_pos = tf.math.reduce_mean(var_samples, axis =0) + tf.math.reduce_variance(mean_samples, axis = 0)

		return mean_pos, var_pos
