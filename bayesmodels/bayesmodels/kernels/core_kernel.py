from abc import ABC, abstractmethod
import tensorflow as tf




class CoreKernel(ABC):
    def __init__(self, name, dim, hypnames_list = [], priors_list= [],
        initial_state_list =[], bijectors_list=[], stationary = False):
        # Inputs:
        #   name := string specifying a name for the kernel object
        #   dim := dimension of the input space
        # hypnames_list := list of hyperparameter names associated to the kernel
        # priors_list := list of prior distrubutions for the kernel hyperparameters
        self.name = name
        self.dim = dim
        self.hypnames_list = {}
        self.hypnames_list[name] = hypnames_list
        self.priors_list = priors_list
        self.initial_state_list = initial_state_list
        self.bijectors_list = bijectors_list
        self.stationary = stationary
        super().__init__()
        return

    def get_hypnames(self):
        # retrieve list of hyperparameter names
        return self.hypnames_list[self.name]

    def get_name(self):
        # retrieve name of kernel object
        return self.name

    def get_initial_state(self):
        # retrieve list of initial_states
        return self.initial_state_list

    def get_bijectors(self):
        # retrieve list of bijectors
        return self.bijectors_list

    def is_stationary(self):
        return self.stationary

    @abstractmethod
    def update_prior(self, hyp_name, new_prior):
        # function for updating the prior distribution of a hyperparameter
        pass

    @abstractmethod
    def update_initial_state(self, hyp_name, new_initial_state):
        # function for updating the initial state of a hyperparameter
        pass

    @abstractmethod
    def compute(self, X1, X2, hyps, indices):
        # function for computing the kernel
        # Inputs:
        #  X1 := input array  of shape N1 x D
        #  X2 := input array of shape N2 x D
        #  hyps := list containing values of hyperparamaters including but not
        #         limited to the hyperparameters of this kernel object. Hence, the
        #         list of hyperparmeters of the kernel object is a sublist of hyps
        #  indices := list  specifying the indices corresponding to the hyperparameters
        # Output:
        #       kernel matrix of shape N1 X N2
        pass

    @abstractmethod
    def compute_diagonal(self, X, hyps, indices):
        # function for computing the diagonal part
        # of the kernel (i.e the array of variance)
        # Inputs:
        #  X := input_data of shape N x D
        #  hyps := list containing values of hyperparamaters including but not
        #         limited to the hyperparameters of this kernel object. Hence, the
        #         list of hyperparmeters of the kernel object is a sublist of hyps
        #  indices := list  specifying the indices corresponding to the hyperparameters
        # Output:
        #      array of length N containing the variance values
        pass

    @abstractmethod
    def hyp_loglikelihood(self, hyps, indices):
        # function for computing the prior log_likelihood given values
        # for the hyperparameters
        # Inputs:
        #  hyps := list containing values of hyperparamaters including but not
        #         limited to the hyperparameters of this kernel object. Hence, the
        #         list of hyperparmeters of the kernel object is a sublist of hyps
        #  indices := list  specifying the indices corresponding to the hyperparameters
        #
        # Output:
        #      log_likelihood value
        pass
