import tensorflow as tf
import math
from .core_kernel import CoreKernel
from tensorflow_probability.python import distributions  as tfd
from  tensorflow_probability.python import bijectors as tfb


#-------------------------------------------------------------------------------
#------ Kernel classes for computing standard stationary kernels ---------------
def scaled_sq_dist(X1, X2, beta):
	# Computes the scaled square Euclidean distance between two arrays of points
	# Inputs:
	# 	X1 : = input array of shape N1 x D
	# 	X2 : = input array of  shape N2 x D
	# 	beta := array of inverse lengthscales
	# Outputs:
	# 	dists := array of shape N1 x N2 where dists[i,j] is the scaled square Euclidean
	#       	distance between X1[i,:] and X2[j,:] for each pair (i,j)

	# scaling
	X1r = X1*beta
	X2r = X2*beta

	X1s  = tf.reduce_sum(tf.square(X1r), axis=1)
	X2s  = tf.reduce_sum(tf.square(X2r), axis=1)

	dists = tf.reshape(X1s, (-1, 1)) + tf.reshape(X2s, (1, -1))  -2 * tf.matmul(X1r, X2r, transpose_b=True)

	dists = tf.maximum(dists,1e-12)

	return dists


def samples_sq_distances(diff_samples, beta):
    # Inputs:
    #   diff_samples := array of shape n_samples x D containing samples representing difference of points
    #   beta         := array of shape 1 x D containing the inverse lengthscales
    # Outputs:
    #   dists_sq := array of shape n_samples containing the scaled square distances

    dists_sq = tf.reduce_sum(tf.square(diff_samples*beta), axis =-1)
    return dists_sq


class ClassicStationary(CoreKernel):
	# Class for traditional stationary kernels. These are kernels
	# that depend on the difference vector r = x-x' and have two trainable
	# hyperparameters:
	#  	beta := the array of inverse lengthscales
	#   vark = the scaling variance of the kernel
	def __init__(self, name, dim):
		hypnames_list = ['beta', 'vark']
		hypnames_list = [name +'/'+item for item in hypnames_list]
		stationary = True
		rv_beta = tfd.Independent(tfd.Gamma(concentration = 3*tf.ones(dim, tf.float32),
		 				rate = 3*tf.ones(dim, tf.float32)),
		                   reinterpreted_batch_ndims=1, name= name +'/rv_beta')
		rv_vark = tfd.Gamma(concentration = 15.0, rate = 15.0,  name = name + '/rv_vark')
		initial_beta = 1.2*tf.ones(dim, tf.float32)
		initial_vark = 0.8
		initial_state_list = [initial_beta, initial_vark]
		priors_list = [rv_beta, rv_vark]
		bijectors_list = [tfb.Softplus(), tfb.Softplus()]
		super().__init__(name=name,dim = dim, hypnames_list=hypnames_list, priors_list= priors_list,
			initial_state_list=initial_state_list, bijectors_list=bijectors_list, stationary=stationary)
		return

	def compute(self, X1, X2, hyps, indices=[0,1]):
		pass

	def update_prior(self, hyp_name, new_prior):
		if hyp_name.endswith('beta'):
			idx = 0
		elif hyp_name.endswith('vark'):
			idx = 1
		else:
			raise NameError("Invalid hyperparameter name.")
		same_batch_shape = (self.priors_list[idx].batch_shape == new_prior.batch_shape)
		same_event_shape = (self.priors_list[idx].event_shape == new_prior.event_shape)
		if same_batch_shape and same_event_shape:
			self.priors_list[idx]= new_prior
		else:
			raise ValueError("Incorrect batch shape or event shape.")
		return

	def update_initial_state(self, hyp_name, new_initial_state):
		if hyp_name.endswith('beta'):
			if new_initial_state.shape == self.initial_state_list[0].shape:
				self.initial_state_list[0]= new_initial_state
			else:
				raise ValueError("Incorrect shape of initial state.")
		elif hyp_name.endswith('vark'):
			self.initial_state_list[1]= new_initial_state
		else:
			raise NameError("Invalid hyperparameter name.")

		return


	def compute_diagonal(self, X, hyps, indices=[0,1]):
		idx1 = indices[1]
		vark = hyps[idx1]
		n = tf.shape(X)[0]
		return vark*tf.ones(n, tf.float32)

	def compute_marginal(self, diff_samples, hyps, marg_dims, indices=[0,1]):
		# diff_samples = array containing samples from the difference random
		# variable R = (X-X')
		# marg_dims = dims of the variable X that are marginalized
		pass

	def hyp_loglikelihood(self, hyps, indices=[0,1]):
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		return self.priors_list[0].log_prob(beta) + self.priors_list[1].log_prob(vark)


class RBF(ClassicStationary):

	def compute(self, X1, X2, hyps, indices=[0,1]):
		# computes the RBF kernel vark*exp(-(beta|x-x'|)^2/2)
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		return vark*tf.exp(-scaled_sq_dist(X1, X2, beta) / 2.0)


	def compute_marginal(self, diff_samples, hyps, marg_dims, indices=[0,1]):
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		beta_used = tf.gather(beta, marg_dims, axis = -1)
		r_squared = samples_sq_distances(diff_samples, beta_used)
		K = vark*tf.exp(-r_squared/2.0)
		K_smean, K_svariance = tf.nn.moments(K, axes = [0])
		return K_smean, K_svariance


class Matern12(ClassicStationary):

	def compute(self, X1, X2, hyps, indices=[0,1]):
		# computes the Matern 1/2 kernel
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		r_squared = scaled_sq_dist(X1, X2, beta)
		r = tf.sqrt(r_squared)
		return vark*tf.exp(-r)


	def compute_marginal(self, diff_samples, hyps, marg_dims, indices=[0,1]):
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		beta_used = tf.gather(beta, marg_dims, axis = -1)
		r_squared = samples_sq_distances(diff_samples, beta_used)
		r = tf.sqrt(r_squared)
		K = vark*tf.exp(-r)
		K_smean, K_svariance = tf.nn.moments(K, axes = [0])
		return K_smean, K_svariance


class Matern32(ClassicStationary):

	def compute(self, X1, X2, hyps,indices=[0,1]):
		# computes the Matern 3/2 kernel
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		r_squared = scaled_sq_dist(X1, X2, beta)
		r = tf.sqrt(r_squared)
		c_3 = tf.constant(math.sqrt(3.0),tf.float32)
		return vark*(1.0 + c_3*r)*tf.exp(-c_3*r)


	def compute_marginal(self, diff_samples, hyps, marg_dims, indices=[0,1]):
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		beta_used = tf.gather(beta, marg_dims, axis = -1)
		r_squared = samples_sq_distances(diff_samples, beta_used)
		r = tf.sqrt(r_squared)
		c_3 = tf.constant(math.sqrt(3), tf.float32)
		K = vark*(1.0 + c_3*r)*tf.exp(-c_3*r)
		K_smean, K_svariance = tf.nn.moments(K, axes = [0])
		return K_smean, K_svariance


class Matern52(ClassicStationary):

	def compute(self, X1, X2, hyps, indices=[0,1]):
		# computes the Matern 5/2 kernel
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		r_squared = scaled_sq_dist(X1, X2, beta)
		r = tf.sqrt(r_squared)
		c_5 = tf.constant(math.sqrt(5.0),tf.float32)
		return vark*(1.0 + c_5*r + 5.0*r_squared/3.0)*tf.exp(-c_5*r)

	def compute_marginal(self, diff_samples, hyps, marg_dims, indices=[0,1]):
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		beta_used = tf.gather(beta, marg_dims, axis = -1)
		r_squared = samples_sq_distances(diff_samples, beta_used)
		r = tf.sqrt(r_squared)
		c_5 = tf.constant(math.sqrt(5),tf.float32)
		K = vark*(1.0 + c_5*r + 5.0*r_squared/3.0)*tf.exp(-c_5*r)
		K_smean, K_svariance = tf.nn.moments(K, axes = [0])
		return K_smean, K_svariance


class RationalQuadratic(ClassicStationary):
	def __init__(self, name, dim, alpha = 1.0):
		self.alpha = alpha
		super().__init__(name=name, dim=dim)
		return


	def compute(self, X1, X2, hyps,indices=[0,1]):
		# computes the Matern 5/2 kernel
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		r_squared = scaled_sq_dist(X1, X2, beta)
		return vark*(1.0 + r_squared/(2.0*self.alpha))**(-self.alpha)


	def compute_marginal(self, diff_samples, hyps, marg_dims, indices=[0,1]):
		idx0 = indices[0]
		idx1 = indices[1]
		beta = hyps[idx0]
		vark = hyps[idx1]
		beta_used = tf.gather(beta, marg_dims, axis = -1)
		r_squared = samples_sq_distances(diff_samples, beta_used)
		K = vark*(1.0 + r_squared/(2.0*self.alpha))**(-self.alpha)
		K_smean, K_svariance = tf.nn.moments(K, axes = [0])
		return K_smean, K_svariance




#-------------------------------------------------------------------------------


stationary_kernel_mapping = {'RBF': RBF, 'Matern12': Matern12, 'Matern32': Matern32,
'Matern52': Matern52, 'RationalQuadratic': RationalQuadratic}
