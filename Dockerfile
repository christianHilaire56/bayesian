ARG BASE_IMAGE

FROM $BASE_IMAGE

LABEL maintainer="Christian Hilaire"

ARG http_proxy
ARG https_proxy
ARG no_proxy

WORKDIR /

ADD Container-Root/setup/prereqs.sh setup/prereqs.sh
RUN export http_proxy=$http_proxy; export https_proxy=$https_proxy; export no_proxy=$no_proxy; setup/prereqs.sh

ADD Container-Root/setup/pythonstdlibs.sh setup/pythonstdlibs.sh
ADD Container-Root/root/pythonreqs.txt /root/pythonreqs.txt
RUN export http_proxy=$http_proxy; export https_proxy=$https_proxy; export no_proxy=$no_proxy; setup/pythonstdlibs.sh

ADD bayesmodels bayesmodels
WORKDIR bayesmodels
RUN pip install .

WORKDIR /

ADD bayesmodels/notebooks notebooks


RUN rm -rf /setup
RUN rm -rf  /bayesmodels

#CMD /startup.sh
